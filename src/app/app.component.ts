import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import {ServicesProvider} from '../providers/services/services';
import {ValuesProvider} from '../providers/values/values';
import { FirstRunPage } from '../pages/pages';

import { CommonServicesProvider} from "../providers/common-services/common-services";
import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

//  rootPage: any = HomePage;
  rootPage:any = FirstRunPage;

  pages: Array<{title: string, component: any, icon:any}>;
  userProfiles: any = null;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    private afAuth: AngularFireAuth, 
    private afDatabase: AngularFireDatabase,
    public service: ServicesProvider,
    public values: ValuesProvider,
    public commonService:CommonServicesProvider,
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: 'DashboardPage', icon:'home'},
      { title: 'Pendapatan', component: 'SetoranPage', icon:'checkmark' },
      { title: 'Setoran', component: 'ListKomisiPage', icon:'card' },
      { title: 'Support Chat Kurir', component: 'ListChatKurirPage', icon:'chatbubbles' },
      { title: 'Support Chat Pasien', component: 'ListChatPasienPage', icon:'chatbubbles' },
      { title: 'Order', component: 'OrderPage', icon:'appstore' },
      { title: 'Rumah Sakit', component: 'RumahsakitPage', icon:'apps' },
      { title: 'Pasien', component: 'PasienPage', icon:'contacts' },
      { title: 'Kurir Aktif', component: 'KurirPage', icon:'people' },
      { title: 'Kurir Tidak Aktif', component: 'KurirnoPage', icon:'people' },
      { title: 'Setting', component: 'AdminPage', icon:'settings' }
    ];

    this.commonService.presentLoading('Cek Data');
    const authObserver = this.afAuth.authState.subscribe( user => {
      if (user) {        
        this.service.getAdminProfile(user.uid).on('value', (snapshot) =>{
          this.userProfiles = snapshot.val();
         });
         this.commonService.dismissLoading(true);
         this.values.isLoggedIn = true;
         this.rootPage = 'DashboardPage';
      }else{
        this.values.isLoggedIn = false;
        this.commonService.dismissLoading(true);
        firebase.auth().signOut();
        this.nav.setRoot('LoginPage');
      }
  });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
