import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';

//import { FIREBASE_CONFIG } from './firebase.secondcredentials';
import { FIREBASE_CONFIG } from './firebase.credentials';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';
import { AngularFireAuthModule } from 'angularfire2/auth';
import * as firebase from "firebase";
import { AngularFireDatabase } from "angularfire2/database";

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServicesProvider } from '../providers/services/services';
import { GmapProvider } from '../providers/gmap/gmap';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
import { ValuesProvider } from '../providers/values/values';
import { KomisiProvider } from '../providers/komisi/komisi';
import { AuthProvider } from '../providers/auth/auth';
import { CommonServicesProvider } from '../providers/common-services/common-services';
import { ChatProvider } from '../providers/chat/chat';
import { SupportProvider } from '../providers/support/support';
import { AnimControlProvider } from '../providers/anim-control/anim-control';
import { PushfireProvider } from '../providers/pushfire/pushfire';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
        }
      }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicesProvider,
    GmapProvider,
    ConnectivityServiceProvider,
    ValuesProvider,
    KomisiProvider,
    AuthProvider,
    CommonServicesProvider,
    ChatProvider,
    SupportProvider,
    AnimControlProvider,
    PushfireProvider,
    AngularFireDatabase ,
    File,
    FileOpener
  ]
})
export class AppModule {}
