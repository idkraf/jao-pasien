import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import firebase from 'firebase';
import { Observable } from "rxjs/Observable";
import { empty } from 'rxjs/Observer';
import { ServicesProvider } from '../../providers/services/services';
import { CommonServicesProvider } from '../../providers/common-services/common-services';
import { AngularFireDatabase } from 'angularfire2/database';
import {User} from "../../models/user";
/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {

  public user : User ;

  userId: any;
  supports: any;
  adsupports: any;

  constructor(
    private auth:AuthProvider,
    public service:ServicesProvider,
    public commonService : CommonServicesProvider,
    public navCtrl: NavController, 
    public modalCtrl: ModalController,
    public db: AngularFireDatabase,
    public navParams: NavParams) {
      this.userId = this.auth.getUid();
  }
  

  ionViewDidLoad() {

    this.service.getPendingOrderList().on('value', snapshot =>{
      this.supports = [];
      snapshot.forEach( snap => {
        this.supports.push(snap.val());
        });
        this.commonService.sortArray(this.supports,'deliveryDate',this.commonService.SortDESC);
    });

  //  this.service.getAdmin().on('value', snapshot =>{
  //    this.adsupports = [];
  //    snapshot.forEach( snap => {
  //      this.adsupports.push(snap.val());
  //    });
  //  });

    this.service.getAdminData(this.userId).then((admin : User)=>{
      this.user = admin ;
    });

    this.db.list('/support/'+this.userId+'/').valueChanges().subscribe(data => {
      this.adsupports = data
    });
  }

  chatCek(chat){
    
    let modal = this.modalCtrl.create('ChatPage', {'user': chat.pasienID, 'order':chat.orderID});
    modal.present();
    modal.onDidDismiss((res)=>{
    });
  }

  chatAd(id){
    let modal = this.modalCtrl.create('ChatsuppPage', {'user': this.user.id, 'nama':this.user.name, 'admin': id});
    modal.present();
    modal.onDidDismiss((res)=>{
    });
  }
}
