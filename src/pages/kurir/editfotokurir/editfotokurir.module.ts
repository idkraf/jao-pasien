import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditfotokurirPage } from './editfotokurir';

@NgModule({
  declarations: [
    EditfotokurirPage,
  ],
  imports: [
    IonicPageModule.forChild(EditfotokurirPage),
  ],
})
export class EditfotokurirPageModule {}
