import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailkurirPage } from './detailkurir';

@NgModule({
  declarations: [
    DetailkurirPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailkurirPage),
  ],
})
export class DetailkurirPageModule {}
