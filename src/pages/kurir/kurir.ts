import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {ServicesProvider} from '../../providers/services/services';
import firebase from 'firebase';
/**
 * Generated class for the KurirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kurir',
  templateUrl: 'kurir.html',
})
export class KurirPage {


  currentUser: any;
  user: any;
  pasienRole: any;

  public kurirList:Array<any>;
  public loadedkurirList:Array<any>;
  public kurirRef:firebase.database.Reference;

  public verifikasiRef:firebase.database.Reference;

  constructor(public modalCtrl:ModalController,public nav: NavController, public navParams: NavParams,  
      private alertCtrl: AlertController, public service: ServicesProvider) {

    this.currentUser = firebase.auth().currentUser;
    this.user =firebase.auth().currentUser;
    this.pasienRole = {};

    this.kurirRef = firebase.database().ref('/kurir');
    this.kurirRef.on('value', kurirList => {
      let items = [];
      kurirList.forEach( item => {
        items.push(item.val());
        return false;
      });
    
      this.kurirList = items;
      this.loadedkurirList = items;
    });

    this.initializeItems();
  }

  initializeItems(): void {
    this.kurirList = this.loadedkurirList;
  }
  
  cariKurir(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.kurirList = this.kurirList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.kurirList.length);
  
  }

  detailKurir(item){
    this.nav.push('DetailkurirPage', item);
  }
  editKurir(item){
    let modal = this.modalCtrl.create('EditkurirPage', item)
    modal.present();
  }

  verifikasiKurir(id){
    let alert = this.alertCtrl.create({
      title: "Verifikasi",
      message: 'Cek kebenaran akun kurir ini. Yakin Di Banned / dinonaktifkan?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Verifikasi',
          handler: () => {
            this.verifikasiRef = firebase.database().ref('/kurir/'+id);
            this.verifikasiRef.update({
              verifikasi:false
            });
          }
        }
      ]
    });
    alert.present();
  }

  deleteKurir(id){
    let alert = this.alertCtrl.create({
      title: "Hapus",
      message: 'Anda yakin mau menghapus kurir ini?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Delete',
          handler: () => {

            this.service.delKurir(id);
          //  this.currentUser.delete().then(() =>{
        
          //   });

          }
        }
      ]
    });
    alert.present();

  }  

}
