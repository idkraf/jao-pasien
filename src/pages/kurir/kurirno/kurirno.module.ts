import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KurirnoPage } from './kurirno';

@NgModule({
  declarations: [
    KurirnoPage,
  ],
  imports: [
    IonicPageModule.forChild(KurirnoPage),
  ],
})
export class KurirnoPageModule {}
