import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import firebase from 'firebase';

/**
 * Generated class for the KurirnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kurirno',
  templateUrl: 'kurirno.html',
})
export class KurirnoPage {


  currentUser: any;
  user: any;

  public kurirnoList:Array<any>;
  public loadednokurirList:Array<any>;
  public kurirRef:firebase.database.Reference;

  public verifikasiRef:firebase.database.Reference;

  constructor(    public modalCtrl:ModalController,public nav: NavController, public navParams: NavParams,
        private alertCtrl: AlertController, 
        public service: ServicesProvider) {

    this.currentUser = firebase.auth().currentUser;
    this.user =firebase.auth().currentUser;

    this.kurirRef = firebase.database().ref('/kurir');
    this.kurirRef.on('value', kurirnoList => {
      let items = [];
      kurirnoList.forEach( item => {
        items.push({
          ids:item.key,
          id:item.val().id,
          name:item.val().name,
          email:item.val().email,
          phoneNo:item.val().phoneNo,
          setoran:item.val().setoran,
          setor:item.val().setor,
          verifikasi:item.val().verifikasi,
          token:item.val().token,
          history:item.val().history,
          rateInfo:item.val().rateInfo,
          timeStamp:item.val().timeStamp,
          downloadURL:item.val().downloadURL,
          reverseOrder:item.val().reverseOrder
        });
        return false;
      });
    
      this.kurirnoList = items;
      this.loadednokurirList = items;
    });


    this.initializeItems();
  }

  initializeItems(): void {
    this.kurirnoList = this.loadednokurirList;
  }
  
  cariKurir(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.kurirnoList = this.kurirnoList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.kurirnoList.length);
  
  }

  detailKurir(item){
    this.nav.push('DetailkurirPage', item);
  }
  editKurir(item){
    let modal = this.modalCtrl.create('EditkurirPage', item)
    modal.present();
  }


  verifikasiKurir(id){
    let alert = this.alertCtrl.create({
      title: "Verifikasi",
      message: 'Cek kebenaran akun kurir ini. Yakin Diaktifkan?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Verifikasi',
          handler: () => {
            this.verifikasiRef = firebase.database().ref('/kurir/'+id);
            this.verifikasiRef.update({
              verifikasi:true
            });
          }
        }
      ]
    });
    alert.present();
  }
  
  deleteKurir(ids){
    let alert = this.alertCtrl.create({
      title: "Hapus",
      message: 'Anda yakin mau menghapus kurir ini?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.service.delKurir(ids);
          }
        }
      ]
    });
    alert.present();

  }  


}
