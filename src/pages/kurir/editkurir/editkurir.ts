import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import {ValuesProvider} from '../../../providers/values/values';
import firebase from 'firebase';
/**
 * Generated class for the EditkurirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editkurir',
  templateUrl: 'editkurir.html',
})
export class EditkurirPage {
  kurir: any = {};
  errorMessage: any;
  disableSubmit: boolean = false;


  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public service: ServicesProvider, public values:ValuesProvider) {
    this.kurir = navParams.data;  
  }



saveKurir(){
  if(this.validate()){
  this.service.editKurir(this.kurir.name, this.kurir.phoneNo, this.kurir.keterangan, this.kurir.id)
  .then(() =>{
    this.viewCtrl.dismiss();
   }).catch( (error) => {this.handleErrors(error);
    });
  }
}

validate(){
  if(this.kurir.name == undefined || this.kurir.name == ''){
   this.errorMessage = 'Silahkan isi nama';
   return false;
 }

 if(this.kurir.keterangan == undefined || this.kurir.keterangan == ''){
   this.errorMessage = 'Mohon isi keterangan';
   return false;
 }

 if(this.kurir.phoneNo == undefined || this.kurir.phoneNo == ''){
   this.errorMessage = 'Silahkan isi telepon kurir';
   return false;
 }
 return true;
}
handleErrors(error){
  this.errorMessage = error.message;
  console.log(error);
}
dismiss(){
  this.viewCtrl.dismiss();
}
}
