import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditkurirPage } from './editkurir';

@NgModule({
  declarations: [
    EditkurirPage,
  ],
  imports: [
    IonicPageModule.forChild(EditkurirPage),
  ],
})
export class EditkurirPageModule {}
