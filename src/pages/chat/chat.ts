import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Observable } from "rxjs/Observable";
import { empty } from 'rxjs/Observer';
import { AngularFireDatabase } from 'angularfire2/database';
/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  user:string = '';
  nama:string = '';
  adminID:string = '';
  message:string = '';
  messages: object[];

  constructor(
    public db: AngularFireDatabase,
    private auth:AuthProvider,
    public viewCtrl: ViewController,
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.user = this.navParams.get('user');
      this.nama = this.navParams.get('nama');
      this.adminID = this.navParams.get('admin');
      this.db.list('/support/'+this.adminID+'/'+this.auth.getUid()+'/').valueChanges().subscribe(data => {
        this.messages = data
      });

  } 

  sendMessage(){
    this.db.list('/support/'+this.adminID+'/'+this.auth.getUid()+'/').push({
      uid:this.user,
      userName: this.nama,
      message: this.message,
    }).then(() => {
      this.message = ''
    })
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }
}
