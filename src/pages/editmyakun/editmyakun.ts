import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import{ ServicesProvider } from '../../providers/services/services';
import{ ValuesProvider } from '../../providers/values/values';
/**
 * Generated class for the EditmyakunPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editmyakun',
  templateUrl: 'editmyakun.html',
})
export class EditmyakunPage {
  currentUser: any;
  admin:any={};
  errorMessage: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServicesProvider,
    public values: ValuesProvider,
  ) {

    this.currentUser = firebase.auth().currentUser;
  }

  saveAdmin(){
    this.service.editmyAkun(this.admin.name, this.admin.email, this.currentUser.uid)
    .then(() =>{
      this.navCtrl.pop();
     }).catch( (error) => {this.handleErrors(error);
      });
  }
  handleErrors(err){
    console.log(err.code);
    this.errorMessage = err.message;
  }
}
