import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditmyakunPage } from './editmyakun';

@NgModule({
  declarations: [
    EditmyakunPage,
  ],
  imports: [
    IonicPageModule.forChild(EditmyakunPage),
  ],
})
export class EditmyakunPageModule {}
