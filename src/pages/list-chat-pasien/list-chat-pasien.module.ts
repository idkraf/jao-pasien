import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListChatPasienPage } from './list-chat-pasien';

@NgModule({
  declarations: [
    ListChatPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(ListChatPasienPage),
  ],
})
export class ListChatPasienPageModule {}
