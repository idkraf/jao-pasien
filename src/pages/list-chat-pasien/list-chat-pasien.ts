import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { AuthProvider } from '../../providers/auth/auth';
import {User} from "../../models/user";
import { ChatProvider } from '../../providers/chat/chat';
/**
 * Generated class for the ListChatPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-chat-pasien',
  templateUrl: 'list-chat-pasien.html',
})
export class ListChatPasienPage {
  public user : User ;
  adsupports: any;
  userId: any;
  constructor(
    private chat:ChatProvider,
    private auth:AuthProvider,
    public modalCtrl: ModalController, 
    public service: ServicesProvider,
    public navCtrl: NavController, 
    public navParams: NavParams) {

    this.userId = this.auth.getUid();
    this.service.getChatPasien().on('value', snapshot =>{
      this.adsupports = [];
      snapshot.forEach( snap => {
        this.adsupports.push(snap.val());
      });
    });

  }

  ionViewDidLoad() {
    this.userId = this.auth.getUid();
    console.log('ionViewDidLoad ChatadListPage');
    this.service.getAdminData(this.userId).then((admin : User)=>{
      this.user = admin ;
    });
  }
  chatAd(id){
    console.log(id);
    this.chat.adminbacaBadgePasien(id);
    let modal = this.modalCtrl.create('ChatPasienPage', {'pasienID': id, 'adminID': this.user.id, 'adminName':this.user.name});
    modal.present();
    modal.onDidDismiss((res)=>{
    });
  }
}
