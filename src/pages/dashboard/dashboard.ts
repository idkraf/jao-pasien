import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import firebase from 'firebase';
import { ServicesProvider } from '../../providers/services/services';
import { AuthProvider } from '../../providers/auth/auth';
import { ValuesProvider } from '../../providers/values/values';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  productList:any;
  orderList: any;
  orderPendingList:any;
  ordernoResponList:any;
  orderCancelList:any;
  orderDeliveredList:any;
  totalPasien:any;
  totalKurir:any;
  totalNoKurir:any;
  constructor(
    public navCtrl: NavController,
    public auth: AuthProvider,
    public values:ValuesProvider,  
    public service: ServicesProvider,
  ) {

  this.service.getOrder().on('value', snapshot =>{
  this.orderList = [];
     snapshot.forEach( snap => {
      this.orderList.push({
        id: snap.key
     });
    });
  });

this.service.getPendingOrderList().on('value', snapshot =>{
  this.orderPendingList = [];
     snapshot.forEach( snap => {
      this.orderPendingList.push({
        id: snap.key
     });
  });
});

this.service.getnoRepsonOrderList().on('value', snapshot =>{
  this.ordernoResponList = [];
     snapshot.forEach( snap => {
      this.ordernoResponList.push({
        id: snap.key
     });
  });
});

this.service.getOrderCancelList().on('value', snapshot =>{
  this.orderCancelList = [];
     snapshot.forEach( snap => {
      this.orderCancelList.push({
        id: snap.key
     });
  });
});

this.service.getOrderDeliveredList().on('value', snapshot =>{
  this.orderDeliveredList = [];
     snapshot.forEach( snap => {
      this.orderDeliveredList.push({
        id: snap.key
     });
  });
});

// cek total pasien/pasien
this.service.getPasien().on('value', snapshot =>{
  this.totalPasien = [];
     snapshot.forEach( snap => {
      this.totalPasien.push({
        id: snap.key
     });
  });
});


this.service.getNoKurirList().on('value', snapshot =>{
  this.totalNoKurir = [];
     snapshot.forEach( snap => {
      this.totalNoKurir.push({
        id: snap.key
     });
  });
});

//cek data user no verified
this.service.getKurirList().on('value', snapshot =>{
  this.totalKurir = [];
     snapshot.forEach( snap => {
      this.totalKurir.push({
        id: snap.key
     });
  });
});

} 
  setting(){
    this.navCtrl.push('AdminPage');
  }

  allOrder(){
    this.navCtrl.push('OrderPage');
  }
  deliveredOrder(){
    this.navCtrl.push('DeliveredPage');
  }
  cancelOrder(){
    this.navCtrl.push('DibatalkanPage');
  }
  blmdiantarOrder(){
    this.navCtrl.push('BelumDiantarPage');
  }
  sdhdiantarOrder(){
    this.navCtrl.push('SedangDiantarPage');
  }
  pasien(){
    this.navCtrl.push('PasienPage');
  }

  Kurir(){
    this.navCtrl.push('KurirPage');
  }

  noKurir(){
    this.navCtrl.push('KurirnoPage');
  }

  logout(){
    this.auth.logoutUser().then(()=>{
      this.values.isLoggedIn = false;
      this.values.isRegister = false;
      this.navCtrl.setRoot('LoginPage');
    });
  }
}
