import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettinginvoicePage } from './settinginvoice';

@NgModule({
  declarations: [
    SettinginvoicePage,
  ],
  imports: [
    IonicPageModule.forChild(SettinginvoicePage),
  ],
})
export class SettinginvoicePageModule {}
