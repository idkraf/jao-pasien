import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import{ ValuesProvider } from '../../../providers/values/values';
/**
 * Generated class for the SettinginvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settinginvoice',
  templateUrl: 'settinginvoice.html',
})
export class SettinginvoicePage {
  form: any;
  constructor(public values: ValuesProvider, public viewCtrl: ViewController, public service: ServicesProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.form = {};
    this.form.currency = "Rp";
    this.service.getSetting().on('value', snapshot =>{
      if(snapshot.val()){
        this.form = snapshot.val();
      }  
    });
  }
	saveSetting(){

		this.service.updateInvoice(this.form.currency,this.form.kodeinvoice, this.form.invoice)
		.then(() =>{
      this.viewCtrl.dismiss();
		});
	}
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettinginvoicePage');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
}
