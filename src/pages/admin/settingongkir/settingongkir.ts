import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
/**
 * Generated class for the SettingongkirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settingongkir',
  templateUrl: 'settingongkir.html',
})
export class SettingongkirPage {
  form: any;
  constructor(public viewCtrl: ViewController,public service: ServicesProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.form = {};
    this.service.getSetting().on('value', snapshot =>{
      if(snapshot.val()){
        this.form = snapshot.val();
      }  
    });
  }
	saveSetting(){

		this.service.updateOngkir(this.form.distance, this.form.addongkir,this.form.ongkir,this.form.perkomisi)
		.then(() =>{
      this.viewCtrl.dismiss();
		});
	}
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingongkirPage');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
}
