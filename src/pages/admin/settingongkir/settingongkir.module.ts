import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingongkirPage } from './settingongkir';

@NgModule({
  declarations: [
    SettingongkirPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingongkirPage),
  ],
})
export class SettingongkirPageModule {}
