import { Component } from '@angular/core';
import { AuthProvider } from '../../providers/auth/auth';
import{ ServicesProvider } from '../../providers/services/services';
import{ ValuesProvider } from '../../providers/values/values';
//import {User} from "../../models/user";
import 'rxjs/add/operator/map';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
/*
  Generated class for the Admin page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html'
})
export class AdminPage {
  
  isLoggedIn: boolean = false;
  userProfiles: any = null;
  userRole: any = null;

  constructor(    
    public afAuth: AngularFireAuth,
    public auth: AuthProvider,
    public nav: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
    public service: ServicesProvider,
    public values: ValuesProvider,
  //  public user : User 
  ) {
    


  }
  ionViewDidEnter() {
  
  //  this.afAuth.authState.subscribe( user => {
  //    if (user) {      
  //      this.service.getAdminProfile(user.uid).then((admin : User)=>{
  //        this.user = admin ;
  //      });
  //    }
  //    });

  }

  lihatAdmin(){
    this.nav.push('TeamPage');
  }
  lihatAbout(){
    this.nav.push('AboutPage');
  }
  lihatTerms(){
    this.nav.push('TermsPage');
  }
  lihatTermsKurir(){
    this.nav.push('TermskurirPage');
  }
  
  orderList(){
    this.nav.push('OrderList' );
  }
  setting(){
    this.nav.push('SettingsPage');
  }
  
  kupon(){
    this.nav.push('KuponPage')
  }

  settinginvoice(){
    const modal = this.modalCtrl.create('SettinginvoicePage');
    modal.present();
  }
  settingongkir(){
    const modal = this.modalCtrl.create('SettingongkirPage');
    modal.present();
  }

  notifikasi(){
    this.nav.push('NotificationsPage')
  }

  akun(item){
    this.nav.push('EditmyakunPage')
  }

  infoList(){
    this.nav.push('InfoPage')
  }

  infoListCC(){
  this.nav.push('InfoListPasienPage')
  }
 
  infoListDD(){
    this.nav.push('InfoListKurirPage')
    }

    logout(){
      this.auth.logoutUser().then(()=>{
        this.values.isLoggedIn = false;
        this.values.isRegister = false;
        this.nav.setRoot('LoginPage');
      });
    }
}
