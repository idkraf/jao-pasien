import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProsescartPage } from './prosescart';

@NgModule({
  declarations: [
    ProsescartPage,
  ],
  imports: [
    IonicPageModule.forChild(ProsescartPage),
  ],
})
export class ProsescartPageModule {}
