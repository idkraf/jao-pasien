import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ValuesProvider } from '../../providers/values/values';
/**
 * Generated class for the ProsescartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prosescart',
  templateUrl: 'prosescart.html',
})
export class ProsescartPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServicesProvider, 
    public values:ValuesProvider,
  ) {

  }


  deleteFromCart(id){
    for(let item in this.service.cart.line_items){
      if(id == this.service.cart.line_items[item].product_id){
        this.service.cart.line_items[item].quantity -= 1;
        this.service.proqty[id] -= 1;
        this.values.qty -= 1;
        this.service.total -= parseInt(this.service.cart.line_items[item].price);
        this.service.totals -= parseInt(this.service.cart.line_items[item].price);
        if(this.service.cart.line_items[item].quantity == 0){
          this.service.cart.line_items.splice(item, 1);
        }
      }
    }
  }
 
  addToCart(id){ 
      for(let item in this.service.cart.line_items){
        if(id == this.service.cart.line_items[item].product_id){
          this.service.cart.line_items[item].quantity += 1;
          this.service.proqty[id] += 1;
          this.service.total += parseInt(this.service.cart.line_items[item].price);
          this.service.totals += parseInt(this.service.cart.line_items[item].price);
          this.values.qty += 1;
        }
      }
      console.log(this.service.cart.line_items);
  }
}




