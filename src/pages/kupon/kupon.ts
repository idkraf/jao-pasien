import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {ServicesProvider} from '../../providers/services/services';
import { ValuesProvider } from '../../providers/values/values';
/**
 * Generated class for the KuponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kupon',
  templateUrl: 'kupon.html',
})
export class KuponPage {

  public kuponList:Array<any>;
  public loadedKuponList:Array<any>;

  constructor(public modalCtrl:ModalController,public nav: NavController, public navParams: NavParams, public service:ServicesProvider, public values:ValuesProvider) {

    this.service.getKuponList().on('value', snapshot => {
      let items = [];
      snapshot.forEach( snap => {
        items.push(snap.val());
      });

      this.kuponList = items;
      this.loadedKuponList = items;
    });

    this.initializeItems();
  }

  initializeItems(): void {
    this.kuponList = this.loadedKuponList;
  }

  cariKupon(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.kuponList = this.kuponList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
    
  }

  addKupon(){
    let modal = this.modalCtrl.create('AddkuponPage');
    modal.present();
  }

  editKupon(item){
    let modal = this.modalCtrl.create('EditkuponPage', item);
    modal.present();
  }

  deleteKupon(id){
    this.service.delKupon(id);
  }

}
