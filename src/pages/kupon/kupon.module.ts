import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KuponPage } from './kupon';

@NgModule({
  declarations: [
    KuponPage,
  ],
  imports: [
    IonicPageModule.forChild(KuponPage),
  ],
})
export class KuponPageModule {}
