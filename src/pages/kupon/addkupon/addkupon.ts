import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
/**
 * Generated class for the AddkuponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addkupon',
  templateUrl: 'addkupon.html',
})
export class AddkuponPage {
	form:any;
  errorMessage: any;
  constructor(public viewCtrl: ViewController,public nav: NavController, public navParams: NavParams, public service: ServicesProvider) {
  	this.form = {};
  }

 

  addKupon(){
    if(this.validateForm()){

    	this.service.addKupon(this.form.name, this.form.kode, this.form.qty, this.form.voucher)
      .then(() => {
        this.viewCtrl.dismiss(); 
      });
    }
  }

  validateForm(){
    if(this.form.name == undefined || this.form.name == ""){
      this.errorMessage =  "Mohon isi nama Kode Voucher";
       return false;
    }
    if(this.form.kode == undefined || this.form.kode == ""){
      this.errorMessage =  "Mohon isi Kode Voucher";
       return false;
    }
    if(this.form.qty == undefined || this.form.qty == ""){
      this.errorMessage =  "Mohon isi jumlah yang dapat digunakan (limit voucher)";
       return false;
    }
    if(this.form.voucher == undefined || this.form.voucher == ""){
      this.errorMessage =  "Mohon isi berapa nilai kode voucher ini";
       return false;
    }
    return true;
  }
}
