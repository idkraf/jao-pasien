import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddkuponPage } from './addkupon';

@NgModule({
  declarations: [
    AddkuponPage,
  ],
  imports: [
    IonicPageModule.forChild(AddkuponPage),
  ],
})
export class AddkuponPageModule {}
