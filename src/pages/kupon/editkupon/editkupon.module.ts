import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditkuponPage } from './editkupon';

@NgModule({
  declarations: [
    EditkuponPage,
  ],
  imports: [
    IonicPageModule.forChild(EditkuponPage),
  ],
})
export class EditkuponPageModule {}
