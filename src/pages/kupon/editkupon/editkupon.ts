import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
/**
 * Generated class for the EditkuponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editkupon',
  templateUrl: 'editkupon.html',
})
export class EditkuponPage {
  kupon:any;
  errorMessage: any;
  constructor(public viewCtrl: ViewController,public nav: NavController, public params: NavParams, public service: ServicesProvider) {
  	this.kupon = params.data;
  }


  
     saveKupon(){
      if(this.validateForm()){
    	this.service.editKupon(this.kupon.name, this.kupon.kode, this.kupon.qty, this.kupon.voucher, this.kupon.id).then(() =>{
    		this.viewCtrl.dismiss();
    	})
    }
  }

  validateForm(){
    if(this.kupon.name == undefined || this.kupon.name == ""){
      this.errorMessage =  "Mohon isi nama Kode Voucher";
       return false;
    }
    if(this.kupon.kode == undefined || this.kupon.kode == ""){
      this.errorMessage =  "Mohon isi Kode Voucher";
       return false;
    }
    if(this.kupon.qty == undefined || this.kupon.qty == ""){
      this.errorMessage =  "Mohon isi jumlah yang dapat digunakan (limit voucher)";
       return false;
    }
    if(this.kupon.voucher == undefined || this.kupon.voucher == ""){
      this.errorMessage =  "Mohon isi berapa nilai kode voucher ini";
       return false;
    }
    return true;
  }
}
