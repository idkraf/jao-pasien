import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditcartPage } from './editcart';

@NgModule({
  declarations: [
    EditcartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditcartPage),
  ],
})
export class EditcartPageModule {}
