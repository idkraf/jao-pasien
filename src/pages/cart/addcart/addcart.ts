import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';
/**
 * Generated class for the AddcartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addcart',
  templateUrl: 'addcart.html',
})
export class AddcartPage  implements OnInit{

  cartItem: any = {};
  price:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController, 
    public modalCtrl:ModalController,
    public service: ServicesProvider, 
    public values:ValuesProvider,
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  ngOnInit(){
    this.service.getSetting().on('value', snapshot =>{
      if(snapshot.val()){
        this.values.currency = snapshot.val().currency;
        this.values.hargadasar = snapshot.val().hargadasar;
      }      
    });
  }

  deleteFromCart(id){
    for(let item in this.service.cart.line_items){
      if(id == this.service.cart.line_items[item].product_id){
        this.service.cart.line_items[item].quantity -= 1;
        this.service.proqty[id] -= 1;
        this.values.qty -= 1;
        this.service.total -= parseInt(this.service.cart.line_items[item].price);
        this.service.totals -= parseInt(this.service.cart.line_items[item].price);
        if(this.service.cart.line_items[item].quantity == 0){
          this.service.cart.line_items.splice(item, 1);
        }
      }
    }
  } 

  addToCart(id, name, keterangan, image) {
        this.price = this.values.hargadasar;
        var itemAdded = false;
          for (let item in this.service.cart.line_items) {
              if (id == this.service.cart.line_items[item].product_id) {
                  this.service.cart.line_items[item].quantity += 1;
                  this.service.proqty[id] += 1;
                  this.service.total += parseInt(this.service.cart.line_items[item].price);
                  this.service.totals += parseInt(this.service.cart.line_items[item].price);
                  this.values.qty += 1;
                  var itemAdded = true;
              }
          };
          if (!itemAdded) {
              this.cartItem.product_id = id;
              this.cartItem.quantity = 1;
              this.service.proqty[id] = 1;
              this.cartItem.name = name;
              this.cartItem.image = image;
              this.cartItem.price = this.price;
              this.service.total += parseInt(this.price);
              this.service.totals += parseInt(this.price);
              this.values.qty += 1;
              this.service.cart.line_items.push(this.cartItem);
          };
          this.cartItem = {};
          this.viewCtrl.dismiss();
  }
}
