import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermskurirPage } from './termskurir';

@NgModule({
  declarations: [
    TermskurirPage,
  ],
  imports: [
    IonicPageModule.forChild(TermskurirPage),
  ],
})
export class TermskurirPageModule {}
