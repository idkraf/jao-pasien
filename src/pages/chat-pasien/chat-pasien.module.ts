import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPasienPage } from './chat-pasien';

@NgModule({
  declarations: [
    ChatPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatPasienPage),
  ],
})
export class ChatPasienPageModule {}
