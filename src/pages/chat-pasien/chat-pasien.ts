import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from "firebase";
import { ChatProvider } from '../../providers/chat/chat';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the ChatPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-pasien',
  templateUrl: 'chat-pasien.html',
})
export class ChatPasienPage {
  adminID:string = '';
  adminName:string = '';
  pasienID:string = '';
  message:string = '';
  messages: object[];

  constructor(
    private chat:ChatProvider,
    private auth:AuthProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public db: AngularFireDatabase
  ) {
    this.pasienID = this.navParams.get('pasienID');
    this.adminID = this.navParams.get('adminID');
    this.adminName = this.navParams.get('adminName');
    this.db.list('/support/'+this.adminID+'/'+this.pasienID+'/').valueChanges().subscribe(data => {
      this.messages = data
    });

  }

  sendMessage(){
    this.chat.adminupdateBadgePasien(this.pasienID);
    this.db.list('/support/'+this.adminID+'/'+this.pasienID+'/').push({
      uid: this.adminID,
      userName: this.adminName,
      message: this.message,
      timestamp:firebase.database.ServerValue.TIMESTAMP
    }).then(() => {
      this.message = ''
    })
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }

}
