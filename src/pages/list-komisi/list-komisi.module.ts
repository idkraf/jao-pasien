import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListKomisiPage } from './list-komisi';

@NgModule({
  declarations: [
    ListKomisiPage,
  ],
  imports: [
    IonicPageModule.forChild(ListKomisiPage),
  ],
})
export class ListKomisiPageModule {}
