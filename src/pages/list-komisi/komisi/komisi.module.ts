import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KomisiPage } from './komisi';

@NgModule({
  declarations: [
    KomisiPage,
  ],
  imports: [
    IonicPageModule.forChild(KomisiPage),
  ],
})
export class KomisiPageModule {}
