import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import firebase from 'firebase';
import { AngularFireDatabase } from "angularfire2/database";
//provider
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';

import {KomisiProvider} from "../../../providers/komisi/komisi";
//model
import {User} from "../../../models/user";
import {Order} from "../../../models/order";
import { forEach } from '@firebase/util/dist/esm/src/obj';
/**
 * Generated class for the KomisiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-komisi',
  templateUrl: 'komisi.html',
})
export class KomisiPage {

  @ViewChild(Content) content: Content;

   myUnDoneKomisi: any;
   items :any;
   ongkirTotal:any;
   komisiTotal:any;
   kurirId:any;

   constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
        public values:ValuesProvider, 
    public service: ServicesProvider,
    public komisiService : KomisiProvider,
    private db: AngularFireDatabase
  ) {

    this.kurirId = this.navParams.get('id');

    this.service.getUnKomisiList(this.kurirId).on('value', snapshot =>{
      this.myUnDoneKomisi = [];
      snapshot.forEach( snap => {
        this.myUnDoneKomisi.push(snap.val());
        });
      });
    this.perhitunganKomisi();
  }

  perhitunganKomisi(){
    this.items = this.db.list('/Komisi-Undone/' + this.kurirId).valueChanges();
    this.items.subscribe(data => {
      this.komisiTotal=data.reduce((sum,item)=>sum+item.komisi,0);
      console.log("total komisi",this.komisiTotal);
      this.ongkirTotal=data.reduce((sum,item)=>sum+item.ongkir,0);
      console.log("total Ongkir",this.ongkirTotal);
    });

  }


  // perintah selesai setor
  pay(){
    this.komisiService.moveKomisi(this.kurirId)
    .then( () =>{
      this.navCtrl.pop();  
    });
  }

}
