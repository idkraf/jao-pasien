import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import firebase from 'firebase';

/**
 * Generated class for the ListKomisiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-komisi',
  templateUrl: 'list-komisi.html',
})
export class ListKomisiPage {

  public kurirList:Array<any>;
  public loadedkurirList:Array<any>;
  public kurirRef:firebase.database.Reference;

  constructor(
    public nav: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController, 
    public service: ServicesProvider,) {

  	this.service.getSetoranKurir().on('value', snapshot =>{
  		let items = [];
  		snapshot.forEach(snap =>{
  			items.push(snap.val());
      });      
     this.kurirList = items;
     this.loadedkurirList = items;

    });	

    this.initializeItems();
  }

  initializeItems(): void {
    this.kurirList = this.loadedkurirList;
  }
  
  cariKurir(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;    
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }  
    this.kurirList = this.kurirList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });  
    //  console.log(q, this.kurirList.length);  
  }

  detailKurir(id){
   // console.log(id);
    this.nav.push('KomisiPage', {'id':id});
  }
  selesaiSetoran(id){
    this.nav.push('KomisiselesaiPage', {'id':id});
  }

  history(){
    this.nav.push('SetoranPage');
  }
}
