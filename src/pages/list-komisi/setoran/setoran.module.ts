import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetoranPage } from './setoran';

@NgModule({
  declarations: [
    SetoranPage,
  ],
  imports: [
    IonicPageModule.forChild(SetoranPage),
  ],
})
export class SetoranPageModule {}
