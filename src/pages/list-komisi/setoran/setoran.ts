import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import firebase from 'firebase';
import { AngularFireDatabase } from "angularfire2/database";
//provider
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';

import {KomisiProvider} from "../../../providers/komisi/komisi";
//model
import {User} from "../../../models/user";
import {Order} from "../../../models/order";
import { forEach } from '@firebase/util/dist/esm/src/obj';
/**
 * Generated class for the SetoranPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setoran',
  templateUrl: 'setoran.html',
})
export class SetoranPage {

  @ViewChild(Content) content: Content;

  listSetoran: any;
   items :any;
   ongkirTotal:any;
   komisiTotal:any;
   kurirId:any;

   constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
        public values:ValuesProvider, 
    public service: ServicesProvider,
    public komisiService : KomisiProvider,
    private db: AngularFireDatabase
  ) {

    this.service.getSetoran().on('value', snapshot =>{
      this.listSetoran = [];
      snapshot.forEach( snap => {
        this.listSetoran.push(snap.val());
        });
      });
    this.perhitunganKomisi();
  }

  perhitunganKomisi(){
    this.items = this.db.list('/setoran/').valueChanges();
    this.items.subscribe(data => {
      this.komisiTotal=data.reduce((sum,item)=>sum+item.komisi,0);
      console.log("total komisi",this.komisiTotal);
      this.ongkirTotal=data.reduce((sum,item)=>sum+item.ongkir,0);
      console.log("total Ongkir",this.ongkirTotal);
    });

  }

}