import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import firebase from 'firebase';
import { AngularFireDatabase } from "angularfire2/database";
//provider
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';

//model
import { forEach } from '@firebase/util/dist/esm/src/obj';

/**
 * Generated class for the KomisiselesaiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-komisiselesai',
  templateUrl: 'komisiselesai.html',
})
export class KomisiselesaiPage {

  @ViewChild(Content) content: Content;
   myDoneKomisi: any;
   items :any;
   ongkirTotal:any;
   komisiTotal:any;
   kurirId:any;

   constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
        public values:ValuesProvider, 
    public service: ServicesProvider,
    private db: AngularFireDatabase
  ) {

    this.kurirId = this.navParams.get('id');
    this.service.getKomisiList(this.kurirId).on('value', snapshot =>{
        this.myDoneKomisi = [];
        snapshot.forEach( snap => {
          this.myDoneKomisi.push(snap.val());
          });
    });

    this.perhitunganKomisi();
  }

  perhitunganKomisi(){
    this.items = this.db.list('/Komisi-Done/' + this.kurirId).valueChanges();
    this.items.subscribe(data => {
      this.komisiTotal=data.reduce((sum,item)=>sum+item.komisi,0);
      this.ongkirTotal=data.reduce((sum,item)=>sum+item.ongkir,0);
    });

  }


 pay(){
  // this.navCtrl.push('PayselectionPage');
 }


}
