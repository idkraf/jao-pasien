import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KomisiselesaiPage } from './komisiselesai';

@NgModule({
  declarations: [
    KomisiselesaiPage,
  ],
  imports: [
    IonicPageModule.forChild(KomisiselesaiPage),
  ],
})
export class KomisiselesaiPageModule {}
