import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsuppPage } from './chatsupp';

@NgModule({
  declarations: [
    ChatsuppPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatsuppPage),
  ],
})
export class ChatsuppPageModule {}
