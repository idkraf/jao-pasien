import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';
import firebase from 'firebase';
import { DataSnapshot } from '@firebase/database/dist/esm/src/api/DataSnapshot';
/**
 * Generated class for the DeliveredPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-delivered',
  templateUrl: 'delivered.html',
})
export class DeliveredPage {

  currentUser: any;
  user: any;
  pasienRole: any;

  public deliverList:Array<any>;
  public loadeddeliverList:Array<any>;
  public deliverRef:firebase.database.Reference;
  sellerName:any;
  kurirName:any;

  constructor(
    public nav: NavController, 
    public navParams: NavParams,    
    private alertCtrl: AlertController, 
    public values:ValuesProvider,  
    public service: ServicesProvider,
  ) {

    this.currentUser = firebase.auth().currentUser;
    this.user =firebase.auth().currentUser;
    this.pasienRole = {};

    firebase.database().ref('/history').orderByChild("status").equalTo("delivered")
    .on('value', deliverList => {
      let items = [];
      deliverList.forEach( item => {

        items.push({
          id:item.val().orderID,
          invoice:item.val().invoice,
          kodeinvoice:item.val().kodeinvoice,
          date:item.val().date,
          status:item.val().status,
          location:item.val().location,
          voucher:item.val().voucher,
          ongkir:item.val().ongkir,
          komisi:item.val().komisi,
          totalproduk:item.val().totalproduk,
          total:item.val().total,
          items:item.val().items,  
          rsDetails:item.val().rsDetails,        
          pasienDetails:item.val().pasienDetails,
          kurirDetails:item.val().kurirDetails,
          kurirID:item.val().kurirID,
          kurirName:item.val().kurirName
        });
        return false;
      });
    
      this.deliverList = items;
      this.loadeddeliverList = items;
    });

    this.initializeItems();
  }

  initializeItems(): void {
    this.deliverList = this.loadeddeliverList;
  }
  
  cariDeliveryOrder(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.deliverList = this.deliverList.filter((v) => {
      if(v.pasienDetails.name && q) {
        if (v.pasienDetails.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.deliverList.length);
  
  }

  detailOrder(item){
    this.nav.push('DetailorderPage', item);
  }


}
