import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ValuesProvider } from '../../providers/values/values';
import firebase from 'firebase';
import { CommonServicesProvider } from '../../providers/common-services/common-services';
/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  public orderList:Array<any>;
  public loadedorderList:Array<any>;
  public orderRef:firebase.database.Reference;

  sellerName:any;
  kurirName:any;
  //id:any;
  
  constructor(
    public nav: NavController, 
    public params: NavParams, 
    private alertCtrl: AlertController,
    public service: ServicesProvider, 
    public values:ValuesProvider,
    public commonService:CommonServicesProvider
  ) {

    firebase.database().ref('/history')
    .on('value', orderList => {
      let items = [];
      orderList.forEach( item => {
        items.push({
          id:item.val().orderID,
          invoice:item.val().invoice,
          kodeinvoice:item.val().kodeinvoice,
          date:item.val().date,
          status:item.val().status,
          location:item.val().location,
          voucher:item.val().voucher,
          ongkir:item.val().ongkir,
          komisi:item.val().komisi,
          totalproduk:item.val().totalproduk,
          total:item.val().total,
          items:item.val().items,  
          rsDetails:item.val().rsDetails,        
          pasienDetails:item.val().pasienDetails,
          kurirDetails:item.val().kurirDetails,
          kurirID:item.val().kurirID,
          kurirName:item.val().kurirName
        });
        return false;
      });
    
      this.orderList = items;
      console.log(this.orderList);
      this.loadedorderList = items;
    });

  
    this.initializeItems();
  }

  initializeItems(): void {
    this.orderList = this.loadedorderList;
  }

  cariNoResponOrder(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.orderList = this.orderList.filter((v) => {
      if(v.pasienDetails.name && q) {
        if (v.pasienDetails.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.orderList.length);
  
  }
  lokasiOrder(item){

  }
  detailOrder(item){
    this.nav.push('DetailorderPage', item);
    this.commonService.presentLoading('Sedang mengambil data');
  }
  deleteOder(id){
    
    let alert = this.alertCtrl.create({
      title: "Hapus",
      message: 'Anda yakin mau menghapus data Order ini?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Delete',
          handler: () => {

            this.service.delOrder(id);

          }
        }
      ]
    });
    alert.present();

  }

  updateKurir(id){
    console.log(id);
    this.nav.push('UpdateKurirPage', {id: id});
  }
  downloadCSV(){
    this.nav.push('OrderExcelHariPage');
  }
  getOrderDetails(id){
  	console.log(id);
   this.nav.push('OrderdetailPage', {id: id});
  }
  
  getOrderPdf(id){
  	console.log(id);
   this.nav.push('ViewpdfPage', {id: id});
  }

  login(){
    this.nav.setRoot('LoginPage');
  }

  deliveredOrder(){
    this.nav.push('DeliveredPage');
  }
  cancelOrder(){
    this.nav.push('DibatalkanPage');
  }
  blmdiantarOrder(){
    this.nav.push('BelumDiantarPage');
  }
  sdgdiantarOrder(){
    this.nav.push('SedangDiantarPage');
  }


}
