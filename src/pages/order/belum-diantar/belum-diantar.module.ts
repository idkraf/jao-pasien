import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BelumDiantarPage } from './belum-diantar';

@NgModule({
  declarations: [
    BelumDiantarPage,
  ],
  imports: [
    IonicPageModule.forChild(BelumDiantarPage),
  ],
})
export class BelumDiantarPageModule {}
