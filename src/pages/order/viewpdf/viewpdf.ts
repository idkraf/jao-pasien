import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ValuesProvider } from '../../../providers/values/values';
import { ServicesProvider } from '../../../providers/services/services';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
/**
 * Generated class for the ViewpdfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewpdf',
  templateUrl: 'viewpdf.html',
})
export class ViewpdfPage {
  letterObj = {
    orderid: '',
    status: '',
    nama: '',
    tgl:'',
    penanggungjawab:'',
    keterangan:'',
    price: '',
    date: '',
    dari: '',
    sampai: '',
    namapasien: '',
    emailpasien: '',
    text: ''
  }

  orderDetails : any;
  public items: Array<any> = [];
  pdfObj = null;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private plt: Platform,
    private file: File,
    private fileOpener: FileOpener,
    public values:ValuesProvider,  
    public service: ServicesProvider,
  ) {

  	this.service.getOrderDetail(this.navParams.get('id')).on('value', (snapshot) => {
		  this.orderDetails = snapshot.val();
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewpdfPage');
  }

  createPdf() {
    var docDefinition = {
      content: [
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAb0AAABpCAYAAACu9v3JAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowOUY4REY5NTU0QkFFODExQkEyQkNFMTM5MTY0MEQ4NyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxRTQ3MUMxNEJBNjgxMUU4OERCM0VFOThCQ0ZEMUUzQSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxRTQ3MUMxM0JBNjgxMUU4OERCM0VFOThCQ0ZEMUUzQSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjA5RjhERjk1NTRCQUU4MTFCQTJCQ0UxMzkxNjQwRDg3IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjA3RjhERjk1NTRCQUU4MTFCQTJCQ0UxMzkxNjQwRDg3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+VV2cGQAAPO9JREFUeNrsvWl0ZMd1Jhjx9tyATOxA7ai9WBsJFqkixUViUbQ2SrZMtT2ybNmnXezTfWZaMzrd5Jwz82P+kTMe+1jT8hmyt7F97FaT3Ra1i2JxE/elSNbC2vcFQGHLRCKXt0XE3Jv5EshCAchMIIFKlOIjb71E5sv34kXGi+99ETfupfv+4n1SLcS0vyn8V3yXXrdH+TulPQX8R8u2xf3EDecoHRM+0eGFBX93wHYdvLMRthvh07XwWTe83wN/R+B9RoLzicljTB4NX1LYJwfbK2Dnwa6CnQY7E1gKzAHzKe4anJ1ed8SZr710tWXnI9d/f+ZrrBUHvreHSEhISEgsHFqDlUcBi4P1gm0Bux1oYzPQRju8bgKLwesmoBMTyEQv5zcxIzmL8hfdYFvBsmBpsHGwfrATYMfADgekmJXNQkJCQkKS3mIiAvZZtEDRIUGhwusCtgrDVg0ILlBulVGuPWnxO0iSiYBUe4KPfLDPgQ2BXYNzX4LtUbC3wD4Ac2UTkZCQkJCkVy9sB9sL1kcJ3YXqDognRIoEpUwnsHLthgOXMw9pzkB+4jolSMt2RzI1wVrANoF5YA+DfQm+8WFAfB+DnZJNRUJCQkKS3nxgALHdBdt7kPBoYQgTVV2B7GYmrkmCowX24kIQxgTxOWxFcd5MvV7ZBSRHiQLUqSmwpWh4rMk5QwKHKSdOJSDALrBOeO8O2D4K9n5AfgfB3gbjstlISEhISNKrhDBQDc7TPQj2dVKcX2spKbpyxYbkJAI2QmKzfUE8MJ8V+Aa5iqN3igI7FggRtkJMl3dIjpxw+B6QJIX/FarArigjVYXoGhh8D15OnouL644QC2wN2D6w42D/DewdUpwDtGXzkZCQkJCkNx0ownCeDocNvwq2E5UUmabMlEmiEiRfIDhQcfAHgFNFEbpKSczQechUeMRUWZOlk+awRk0gL7VAemJqwJMWfScZvJl3GEnbPh3P+TTtMsX1OXVcTmyXUbtIhAp+34BSasCApXKIqSFRIygvGg6B4pzfz8B+RYpeoBISEhISkvQKQIeUu8H+iBTn7nrKPywNW3IQcB6oOK9AdKDPBOVARCJqaqw1qntdzSbvabZoV5NBWqK6SER0Gg/rWtRUqQa0hccR01YO0CJxCc/nPOsyPpb12PCEJ1K2R66M2aI/aRP4W0/mPNXxhJqD8yuUAwEqVNdoUQXS4lhmmYrsCQyJG9cR/CPYe2BJ2ZQkJCQkfntJD+fn0DHlD8AeBO7ZNZ3sCs4lXBAPCc/nwgNVZ2gKb4+Zfk+zyXrilljXHqIrEyHS3mQobVFDbbZUGjbVknos9+icC0hZvMCtwKc5OOFoxmVoQHrs0lieXR616eUxmwykHRUUoZZzuALEpxgKqEiVFOYDxfXk1wV/fZsUHXF+BPYCKQ5/+rJJSUhISPx2kR6uqcM5sD8Be2SmHXDujME/jssFiDseMRSvN2H5t62Iic3dYbK2xVJA3SndcUsLyqgsoDy0RJI4BwjqECxE1rSGkMLQH4YNpmx+JWmz8yO2d/TqhHdiIKMMpF0DeFEzVEoNHciPzsit6ISzDew2sP8I9iZYXjYrCQkJiVuf9PBYa8H+EOjhT4FR1k1xTnHsEWfdCmQHagtUFwPV5q9uCbGdK2PuHatiYvfqZj0e0XAOzViCa8ciaSol2oqERdDu7iVufzJuf3xpwvvwUpqdupbVro072kTe1zRQfqamFrxBpy2BMOH1P4NtLxzw+7B9BWyQSC9PCQkJiVuW9NRA9fxPpEgA+tRHojCciSRhu0B4PuMRXfE3dEW8Pb1x/94NcbqtO2YBmRgLVHT1gNGTsNDY57a2uqD4vHfOprw3T6e0K6m8nnVA+WmKYuq0UFB+/Sp5nOP7K7D/BPb/guFCdyabmISEhMQtR3rifvjn3wIHfKFEXOURUXxfENtjggnKWyOGs29Lwv3yrg51Y1c0rCpEo5Xn5ZYaathQQrevaTK3dEf8Peua8y99Ouy+fWbcGMl6FuNEtQKv0fKFfoKINtjsJ8X1fj8gxdieQjYzCQkJiVuH9L4Ovfr3SNFL8zqlhsOZOJSZczmPGaoLqs59ZEcb2d4Ts+JhTUc/kUauHCi+EjZUo29Nk9rbGvL2bpiwf3l4mL1/ftyayDMjZCrE0ouXXDaWiWHO/hhsFdj/SYrenRISEhISy530QNl8AzZPgu0uP1awXIBkbEayLmObOsK5r9/Rwe/fnDB6mkO6rlKVNJ66m132KVRtazKUz0YS2vq2sHvg2Ej+nz665l0cy4diIPmi5g3VmIC6+TIperEi8b0mm5qEhITE8ia93wP7N6Q4l6dOEUTRJXI85xMD1N0Xd7TbX97eRnevabKaQwUnFbpM64oaqqKtaQsp3+jrVte0hp2fHh7Kvn8uZSWznt4c0Yvr+qYGM3GI8yF4Kwpv/QW8/gWRSxokJCQklh/pQUf+u7D5X4P4lJNzeOiwknc5SeU80RbV7a/s6vS+sqtD620PGZqyvNTdHFDQw/TBLS3KihbTSYR1+8WjI3xkwjVaojo1FFqIBxpcqCGK2SPQoQVTGb0um5yEhITE8iK9z0GXDgpP3DlJgrSYVDXn+CTvcba+I2o/uquNfXlnh97RZJjk5ntl1l31aSrVt3ZHlT++p8eJWZr9i8NDQHxeKBHVCCjC6XE8kfj+Z1KM1ynn+CQkJCSWAelh543K7t+Cfab8TfxnwvZxsTnbsTKW/8O7u9m9GxJmzFLNW0TdzQa1tz1sfesz3SRiqt7zHwzS0ZxrxEO6Ymi0nPiwnr8EliHFkGUyVZGEhIREg5PeBmCvf43LEgQRk6sMMEBXzuHopclu64naf7K3B9e4hUD96bco4YnASuoVo8eEvtHX6WiUui98MsQG0nY4RjVaVHyTzIf18TVSXL/3l2AjsvlJSEhILC0UUaCtioY55jDW5DfItOSueU8Q2+FsOxDed+5dwR7Y3GIB4S1nh5XZ2Q74ayDleEeuTLijGdcnZRmR2mOG+didncYf7+3xO2KmnXWYwADa9HqpHKXF8GzfIkXPTgkJCQmJpSS9KvYJgz0CvfufieLr4hdpISMCSeV9sbLFcv/grm72+a2tpqpS81arJFaUa8LxGPv54SHvrw9cZO9fGMdUf9dl8WuO6Oaju9utr+3ucFvCupcB4sOwa9PCdmKWhj8De2CKDOf+T0JCQkJiaUgPP98ZZBRYUf4mAx5I20y0RXTnK7e3efdtShjQud9yhJdzmThyNeNcTTruSMbz3j03rrxyYkw7dDkjMrY/PcwYtQzVBNIzHtneaod0xc27jAhOJkOxBSyJQaq/Q4qxSiUkJCQklor0KgxsdsA+vw/20JQqKXbcGZsTS1W8R3e1u1/b1anHLO2WVHjHB7LOD1657L3w8TXv7EgOsyBpYVPVRjOeGEq7HsP07NPQHTdDoHyV+zclPFVRfNvjhaHRMs2Gc6mfJ8WhTlM2QwkJCYmbr/RwXd19YF8skV1pZ4dxAh05u3dT3P7q7nYV57PIrTeHJ1yfs5ODWX7w4rj12okxHVSe5zPhN5kqGUo7/JXjY+7RKxkbnXimf7knboUe6+ukO1fGHBCLDMOxlQ9zwst2sEfh5Z2yGUpISEgsDeby3txCChnPxZbynpr5gmRtzjd1hvNf2dWurG0PNUJ2hLoBiE6M5XwGGs7TVCpcj/vNlq4PTDj03bMp+FyIWEijF0fz+onBrHJqKMMff3C1s7EjHJ7+0LBjZcz4+u0d9mDacS6N2RYcT9HV65YybCbF+b3TYEOyOUpISEgsMunNkgIAPQvR0WIPMJ1SHNCkhbAiOd8XIUNxH93dwXatarI0RdFvpQpJZT323z8YdN69ME7XtoYYXJ9oDoGyyzANPoshcSkKoS7jRhYY0POFQ8XMtagoVL9nY4KdGsq6P3x/0M84vtES0QmdyseHRPlQYM8RmYpIQkJCYlExm0LbBPZVsE4aJIBFczH5KyP83vVx54HNCRMo71YiPJybY4ISd9xh/JMraf3Xx0ZDH18eDzmMqSawH6VUB7JScXoOfTItTROdTQZpixqzKt2oqZoPbW5Vdq2MOsCNLOtcNwWII549ggjMyrAB1z+iQ+j0/yQkJCQk6qT0ZngP1cc+UpxrUkpdLi5PcHwmWqO6+8WdbbQnXkgtcEsMazo+5++dH/fODmXRKdXPOowkQgbNuaDuch5RQdppytSiPHRdQScXS6d+TzwkmsLaXCmS6IauiPGlnR32laTjXhrLW5Zu0LI0fPjgcBeQG86fXiTFUGUSEhISEktBerSYNQEdLFpFIEVw9M72GDEUxbt/c8LeuTIWAuGj3gLXX1B3rs+910+M8V8dHVGbLNWMWhqJmIqCRDd9kR1mUnAYMCPYurYQX9cRoiAB56wLXaX63vVxdvTqhDf8iatlHKbHTLU8/yzm4MOF/++AHSMy8ayEhITEkpAeemFicOTtpMwbEzp4AX08X98Rcr+4o0PBlHhkGXtrunA9A0mbXQLjTLC07XtXkjbm/tPzLjNgS0wdmAwYTkxmRS89FVCSc31cnM93r4yJbd3harJH0JaIbjywqcX/+PIEenxqQHq0jPWQXlHp3QV2Vqo9CQkJiaUhvS3QB98L26ZCT4zemrww/EdilurftynOtnVHQ6pCl/OwJi4qt3/96aj3q2MjuudzFWQrcbng7TGDguqjJXU7HVgfji9IHlTvpvawv3d9M21HyVYdlM3dEX17T9Q7NZjzbJ8bpazrASKk6NDyFil6c0q1JyEhIVFv0iuXKNDL3kOKw5uTZIixtoD4xNq2sA9KBbMHLFuVB9ciRjOed+hy2j14cVycGMzocCFqWFNFxNSErisUsyMQMTPj0CCbRNTQ/Ie2tvFdq5o0Un3QbopLH/auj3tHrmbc00NZ3VBBTcJB+VSFfg7O+0tSDEot1Z6EhITEIio9nFe6ExdNl97A9WQeMF7IUL1t3TG2oSNS0yJ0VEsCoCilBEQ3V+GNpF33v3006L92ckyzPa51RA21ENwyiAuNzjqzFRIJL+Oi96pgd66Luw9va1MTEb2meU1FIcru1U3KnWub2NmhnAcK2ghDEcoYFuNyogPRS5L0JCQkJOoPpRQPEuxu2OyArVnIm0MLc3mojviKuO7tXBkVqkJvWKKAno+YUQAXdZNpAgmV1cC4gx28m3EKcSpv2pAdRpA5PpD1Xjw6qnx0cSI0YTMTlZYGbAbXBTY7K9NA8WbyTPR2hPyv3dFB1raHkPBqHealibBm3NYTVbuaDd/2pgJST4vLuYoQGWlaQkJCou6kV9avoyPFxklJRgvRSYiuULZjRZO4fU3TTEsU+MnBrPvzI8P5t84kneEJ13MZ91kpiRwV4jQomr957ZL300PDHn7OuFhy4ivE0OzPsl8cGVFTOc/qjJuKBkTHRJUsXBjWZKQtprtf393B926IG7pK5+W9qgDPgmJWNndFBTwGUJ/fUIIdpBAUoDDHJyEhISFRR5SGNxPQs+OC9DDSAKaI9Ytr0Xh7TPc3d0Voa0Q3ZurDJ2xfvHsm5QKh0U1dYW336ia1tz3Ce5oN2hTWyaqERXHU8L+8NyDODefFF25r87aviCiWjr6RS6NmxrKe//LxEfab00m4BkGbwyrhvFptRkje5ah8/Yc2t7CHtrWpEUOtxmNzVnTHDWVLV4S9cXLMB+LTpg2SdgfEFyPFTOsSEhISEnUmvbVgnaW/Uc5hUGkUQt3NJlvTauHbM0ZfuWd9wkpEDPqLw8PivbNJ8vaZFBClybd0R8Uda5vUrd0R+uf3r6LPfTDAXjkxJo5eSZMv7+ggD2xJKCtbLI0SuiACqQTb4/77F1Lee+fTGudci1oqqUVrer7ArPB8b2/c/d3bO5WuJnOhCXJp1NS0jZ1hvydu2gPjTphxWlCeZcVaD9YGNiCbqISEhER9SQ/JbCt0uZ0B3xXmmHCuSVWo6IqbFIjvui+BSuLJvMtAAQlDVdRVCdP45/et4I9sb/XfOZ1i755PKW+eHlPfOj1G1rWHeN/aJnHHqpiPaXbeODWmPfv6Zf3jyxP8q7vbnbvWNWthQy15hNab/PjJa1n/J58M07NDOS1iKcRQi8OaVQg84gM75lxf9LaH3a/uaqe9HWEd424utFBwACBPg+Jx+8fdwryeXrYmELCGFOf2TuHzh2ymEhISEvUjPWS0PlFUegWIgtemEFFD4+vbI6Sz2bzOLd9lnL9+Ysx+/1wa9aAOn9MVcZN0ATnuWh0ja9tC3oXRHDkxmKOXR/P8v743SFqiuhGzNCVmqbms4xMgv8iZoay4b1Pc/Z3t7WxzV1QP5snqtQZQDE+47qvHx/xjVzOWqhEFTl+1ysP9mA8Xp6hs39ZWftf6uGYi5dUHtCNmKjhs/P6FNGOCo99QeeyXNaIYIOAVsGHZTCUkJCTqRHrQ0XaKYoDpSEnh4IJ0UHOiOaT5K+IWem1eN+sE5KFtXxHT0Tvz0pjjjWU8cnEkR0GxKDhVZ2qKqqmEondkLKxNXJvwMqmRfGvExOkwxQiZuBqcaVdTDvnJoWHlxEDO/+zGFu+BTS3eunYk2Ekv0YWoKn5hJM/eOZuiGYepzWGtwCpcVFZ4CI+j5yoRq1tD/u1rmpSmkFrX9YlwPG09KMeoqfmpnKsxQcrXdYRgu1YUf5M5Se/h//vDW7Fd9gZWjiTYQXnLStziKLX9vrL3zgXt/4Csnuvx0vdqT0eKqYU2QgfbJYpJYwvE4EFvD7TEexIW72o2sB+eJD3gOTGS8diE7dMmSzNWJigL6wq/liZ8IOV4F5M5dyLPuK7QcMRUmkC3CUtXNKoQw2Ncw5E8ZNqwqRRe5D2uHbqSVq8mbR+2/ubOkL+mDee7LKWzyRTxkKbA90sKsOohUFCq/vGBDLk8ltcDAg4Ir/T1Wdgv0FsuZ5gaiPStjuHwJs491nXoVVMVpT2mKy1hlaVyAuuV4EL1slJh1vpWsGtg+TqeOgG2P7ixEgGRPB/cWLVif9nNicd5doFlewzsiWk3/HQcCGy+ZV6Mci8E9ShLb1B3fUHneC44TrJO30+UlTOxwOud729X73qq5joOBnWwkLZWDfYF5XqsinKV7tdqf99arrn8wfJAHR4yF/qbVWx3NTz0F9odkOQ5VFWY0iY25UlJC0sVQJGRVS0h0lYMszXZ4XPonc+P5O1XT4yysaxXSLgjgCNtkEaewL4c2EEhzPa5bjOmgEqMIAHqBUeNYq9ezM5HC/NY6MDhMUrHcp7+5umk+tHFcdYeM8BMiqQHxMASYY01wxGaLJXGLE20RQ3MTI7L65QyMrxO5R2+PMHeOpNSgMD1EFxCLeskkIDQuzMeVvmOlTGSCOuLEXYNr0XpaDbYuZEcYSD1VJ0Wcu0FlNxMiuv1UqQYj7NeT5EfTmtAeEM8BfZ4jY3ymaBBlqMvOM58iPiZoCzVdBD7gjI/DfZkjeeqZ7kXinqUBevsuRnex4eHh6vouOb6/p1BZ/9ShQeRWjv4asu2FPVUqaxkAW2tmvvxmbLzVIO+wJ4IyvR0na/5sWkE++Q8FWY9frO6tzsgyYe1oOKj5cQBKonELIWgyouHNWW6EILOWt3QEUYnD+56BJSfS/rHHQUDNedcYQDvmZZOVS6KAbZEoJ/Kx+9Kc2v4gaFh6h4Fo78oji+UCyN57fyIjcsEhKmpHI7FooYqmkIaARPdcUts646Kde0WMYFN2yKGBkRZEFD4D4Ya++XRYQrEp+vAqrBP9R6bophBAegHVK7FVraYGItzMbxLachQlM4YHp/iw0TwMDAJ/E3WgqXrSHr753jae6IG0ntihgZdOv65CjdiPRv3E8H2yZtU7oWgXmV5qsKDxMMVFMETc3y/1Cb66nztiaBzPdgA9VTL71VKuVave/GpBSjnRPB9LNM3Z/mNF3rNfcG9WesDcT1+s77FanfIK2uQx8rDqeA2pCusNaqJ0DSRg8puY3vYemBTi7V7ZZO6tjWEDiy8OaQyTaXo8IgjoBhjBFPu4GLsWRijeMZiqLJiBBg8VxRUWdTSaNhQKChBxedcS9vMuJp0zGP9GeOdcynzJ58MWc/85pL2l7++IP6fly/yv3un33vj9JibsZkHB/XfOZtib59NaXmPofPJtPOLWYc2aRnpA1fyNa0h1hbV6+lccx3g2YGCkoWnBEUwcUPpMK/hykDt1Qt9FZ46ayWb2W7mWvDUAhv3Ezep3PUgvXqUpbfC7/3cAttEYpGuv6+B6qmWMj9TJ8J7pk51uy8gpsQiXTOZBznX4zdbtHaHwbd6oJO1Su+g+zxwFg8bGk+EdTF96BDntnSN0v6U4/38yAh77sNB8etjI9rhKxlrbMKNKEKE1Ot4pjqJhXthx49fxPm3EgHGLJUAodKmsEqLZKgC7wplNOPqn17NhN47O249/8GA/u9evaT+8siQd7R/wnnnTAo+99QIfB+XwPMaaqRYDoEKkqwDQm+y9EVbQ2iCem1rMoShoQ/QVFUFqhgXp28Fu400FvZXaJC9NTTs3hpJayYkb0K5G6kOq+kUn1mC+q0VB5fZb1Zepn0L/P4zdS5T3xzEVy+FtH+J2/aitTsFKA4jgGilzjYIoSliOJRo6fj+DeG2GNIiFZhFncP+7vkR2780ZqtZh+mg8haUUR3PjkUoWUkJgmwkmAEBFCAOcZLmsE6gjBSIUQFpqZ0azBl/+3a//tcvXVSPXJ1QLZBqhdQ9omrendwVrgGTyIqVLaZiIQMvEgyVKG1RA8s5GRGNXt/QMDTcIw1Gek/UaR9C5p7DK3mrVZpPeP4mlLuR6rDajmi+5HCQ1N9rMFnlcNlS1VN5WztAKjutzPecvYtAeOXE98QCrrleSrlev9mitTskqKn5PFpwVEHPTZw/E0AwM0ZLUQUl21fErI2dEQrCy7uSygvXZwLn5jDT3qJE1xTFNOflBIZOMApwkmXgCghBr6VdvX/c0SKGVkgCO6/TFNWmCOmUI7EqdPGixahQWUh6UajroYwnMPZ0GcVi3cfqOERRDzxWZXlKHmPPV3GjztY410972nuMTHm5JcpujCdvQrkbqQ6rxTNBZz6fjuSbs5Bmpaf22TrUarwhl7KesB09PEPH/NQc6jkxDzXyTA3lOVB2/L6yc1Yik2pJbPo14zk+XCDp1fs3q3u7C7w3CSu9QwOVha76IKKoptKZPCMLY5zopBnSVWNNq8U6YyYfTrsknfeBgFRScH3B7AWBS2j9OPDG5QalYUF090+ENZxQpGKeqwsoKRE2FaauiiZLXdTg2FhME+pZV4u+MgVVS4uerWXXmWsg0ttf476VGvVsN/G5GTqU5wN7vGx46cBNKncj1WEteI7U5jVZ/hDy9Cydf6XO5+llWE8kKPf+OTrxvhofIPpI5WHRc0H7PjDLvfJEFSpp/zwfbEpEu6+B2va82x2Q26ztTrlRUOHCAiAsVSmk3KkkVvasjZt/fv8q9Zt7ut117SHb9biXyvos7zLueKxASnSJkuTwgEkWejpaWFtBFXTKCR4KWPnDQV0FbEHdKWIZ5BHaV+MNsW8BN1DfPJ/kbna5G70sJY/OhGxrVaGea/P2V3GuO+do18lgVKPSyMZji/T7HlhG99mcUMg0KUcDgVGaS6uEiKXq96yPh//Fg6v0f37fKrZvW6tz1/pme3NXxAPq8FM5T2BanhL5TY+3VSNFkMVMyVfKI4iL5tM5Rl85MSoOHBv13zqb9A5fmfAGUo7LOalnXkCKSypwKaQKFMuEWJLrnCceW6LvlA8FvVSHG2Opy93oZSk5PTQyGuk3qyeRz4XHSXXDpU9XQUDzrYveCkrwlvjNNLLA7hX5S1MpTYR1/f5NCeX2VTFuM877xxz+0vERfvTKBBvJemoqzzC9AQ6ZElwEoKtKYU6uNKS6OF18hegrMz0FFIYcFZp2PPW/H7xmaiB3DU0R8B5f0Wz4921qce/bFDfaY6ZRjxIaioLxSHF9R6EicFsIl9ZYN+x8veT2BzfpuTmeHvdV8TRYaxSKxS53I9XhfInvGXJzFuQvl3qqNBxZyxBiogKhHKjxeE9XKFuiyjLtm1Z/vfMsXyO17epIr14Mg+HGLN0oyJTuJpOs6wjxa+MOe/tMynnx6Kg6lncVxxNqzmG47IAWiA9UFcZ8UYIQXPV1ghFl5Ff90okgRqdi+0zxHSaK822cnBvKiiP9We/SmM2+vbfHa43q+oJLSEurFWul5yXFXPMIycB65/jubJ3r88HniSo6ILSnyNTc3vM3sdyNVIfzxf6l7mwauJ6mE0AlT8ha50QrrUmsdQ6u5OSSmONh8ekqylSN4sdr/eYya9tzC5sZtRElC0n0g99S0AmmI2ZoO1bGtN/r69C/+4U14t88ss7/g7u67D3r4rmOZtPmQviZvM+zLsfMDUXPyWD4s75zXKLmvQtOJiBJI4aKkVNwfSANmaoyMO4YPzs8rL56ctTNe9xbaMFcxjioYAxDVljMX3AdbaxOqNIanWcrENBc63aw0601tFMptFKldUmLWe5GqsOF4Cny29PWqiGAklVajF3vWK0Hl+g7tQLvz0oRfRq1bVdPepPMVT/iUdtjJg59Gl/a2a5/a2+39j8+tFr57sNr2J/dt8K5Z0Pc1ij1RjKemHAYcXxBfF9MDXnShXLwAqQrVhDUEAjSgkcrLliPh9TC0ogffTysvHd+HDMwLcjBBfMK5VxOC3OeCmlEVBq2qEZ1Vbop5vOkt68C8S12uRupDm8VLId6OkBuboDypQSqs7Nk7qHUZde2Z+xmUXEB8aBjRd0cNpD80EekLWKYt/VErYe3tprf3tuj/vn9K+nv39np37G62TFUxQHl54/nPJHKeyQLJOgCATKxSGv/qig0ntaFAmSgLKmsVwjGzTinn1waN3788aBy5lpuQY4tGNyazX1xN5MKExWGLs4FT5wHKzx5VhrCxE5kfTAkU8uc3WzDUEtV7kaqw+WO5VBP1Qz1LWV9LdV5ZnMoW5Zt+7p1eAXvRVA0GBMr5wLheGKx3AgL/i8hXbV2rWoyv3PvShXUH/tnd3Z5+7a2un1rm93etrATNlTX8bg/nvdFBsvD+eSXF5vsUOlidJqJvA91wTHgtbepI+JsXxlz1rRZngMfHrsyQS+P2fP2OcHaxezztsuC8854ZfpNvLEquT8/O+2Jbr5DIKUbBIc6W4KOpVqnlZlumKUsdyPV4Uyd9HLJQdhIv9ls57+TLE54rFpjzyYqfKeaMk6PyFLpO88sw99sRmCeuLwgU9GOkfSgF8fEq9QusgyaupjE2xTS9D1rm9WdK2JiPOez0azLr4y57Gj/BDk7nFPgbzBPHc97KlMELorHpYSLov5Kx3U9jiqTRw2Nre8M+zt6onznqhiJWir5tD9L3jiZpB0xnXQXs8rPi4fh4UKMZTwOirYQ+WbaUg4fLAt2+SZ2RJUWwj5Fqp8XKnlqVYPSkMiTZCrHXqXAygcaoNyNVIelju2bwRBVo6vERvrNZiPlJ+dJegfqXN5Krv7VPOhMj8iC7eM5MvtQ5kxRVBr9N5uZ9IDwsFPtwgw/RRYs5HSjQD4imS9IEG2RSa8krlScM+toNjDHnNjSRcSedTEog+engAg/upj23zidpFeSeQ3IwkCvj3orPiWQtTYQnsc4X9MSdh/a2sIe3NJKu5t1E8i5MMO4uTMq9q6Pe5ghvqvZnLcSg2cKMZLziOPPSHpjYO+BHa3ycPuCzr+XTEU0Wcjcw35S3xBoJbfmWspUuobnydz59spJrxHK3Wh1+DBZ3IDEC8XNrqcDZffQXGrkOXJjuLJaVHffHOV9ospOv9KQYrWkN1M7qbQUoq+M9BrpPquN9MAugm0jAekVPAiJUDKup2GGdIxKQpfWg6S4YB7+jUd0NCQEvq0nwvesa3Z+eXg4/4sjI0rWZno8rM7BoYF0rUHh4cXmXHgMYILvWBF1v/WZFWxvb1y3ijFIJ0+Gam+jFVbJAkdabZfRobSruFyoJSeW0hAnlGIcNh9VSXrPzDI8UMq1Nd8n28Xo3J6dYUijlJ06WYH8HmuAcjdSHdbS4T5Oak8oulRohHqqRhHvq4GcZhq96Kugis6RysOAL1UgmySZf6DmSt/ra9D7rFaCoVdwGG3SVyRwk3R8QZNZj2ZddrNvCKQDzdRUY+fKmPXA5la1NWq4IEIZOpXMTGu1TUXSIBlDwXEGjrtjRcT908+u5J/b0mIA4RmzKN2FPgqIvMvZtZRbmEMthHwrrtkjwXCzCwQ4AjZURUPcP0dn8ti0p6lqVeNihAgqX/D7UtnwxzNBhzOXmks0SLlJA9VhrZ3uk6Tx0Cj1lKzyAfGpBdR/JTwX3AO9sxDCh6Ty/N/TC6yz5di2ayaU06SYnXtyYg8Xjdsep1eTDs45sQa6QfQt3ZHQ/gdWKl+7vSMfMlTXBjE6XyVacinFRQfJnA/XLPzPgJrc/+Bqfu+GBJLdojqR5DzOR7IOZyAx1RuXiOBNeIZUXkBciQyeIVMhveqZHmS+KGWg7pvhOvYHN/5YUN5SgN0SMVYazlnsci/m/ktx7KdJ47nbN1I9HajyweA5UvtQcbUZw/cHD4BnydS6QTEHGc40IrIQpUWW2X1WMzSoz3PXkx4hmCLIcTnpH3fIWM7lq1uthrlDmkOatm9rK93SFXV++IFmv3ZyVOQcZiJR10J+KGuBc4hTdFjBGKLsro3N3h/e1U1uX91kkPpGq5mxCDmXibGcT7HiVUWZ7pgzLoToh+3wAs9TrWdUskwNLubT1j5SXfaFap8mS8M5S1HuXlJdFJNGKst0PE6mItzcbDRiPT1dRdtLBA9ita4vfZrUln6n1jmzWhxtpidwfqyKNpFs8LZdLekVDo4da8FLEztezJtqC6JcG3e0oXFghVUFT0KtAW6SArFpKlVXtpjmrpVR7+CFcT+Z9U1dnVvRlfOJCP7RFcqjUcPrajb9z21pZZ/f2mJ0xgxTVRZ/mTgIVH8w5fCRtKsplCrqjXkIUe1kwJwqnk6TZOFOCs+X3QyVnlgr3eyVnkrr6VDx9BKWuxbSa5SyzISSY8vNJr5Gradq5vf2z4P0SkOoi+FU9GyNKq+X1B6R5/ll0LarIj2cNzoFXe79KKTwTbUYCkxJ5nztcjLv+owzTVUagvQCsNODOe/NM0lzaMI1TI1ep/Imo8kUY2gW5szwPcy5Jwp/ExbSFaen2XDXtodEb0eEb+6IKoaigLgSdAliv4hUzmNnhnL+hM0sXIAxjZiR7C5CMSZq6PgXGlKqluUEB6q4AZ+qMExysA6d7sEabvR6lLteuNllSQYdUyN7dN7Meionp3rjIKm/N+18oxrVglJG+d5ldJ/NCIzpn4ftx9DhXiNlHa8KRIIBly+O5shwxm2UoP8cyML9zcmk8+9/c1m8eTppAEkpplYc2gxy1xaIDqOo5KDYmTwjaZuJjM1I3sNoKgVKc8OGkmoKGfnxrG+8eyYV+8GrF8P/249OiZ8eGrYZqLDJEy5SJJjhCVecGMgSn3OqBWRctjgdhzWPgaVqIKyFjOU/XvZkdaDCU9azVXZWc3UoB6edcz4oPVEml7jctXQQS1GWZIX95up8KzluJKv4DerRkTZiPVWa31uIwwiW6U6y8MABpRx7j8/z96v1IaBR7rMFtTvkCaAH8QktdrSi1NHruEidC3plzCFgnNzcOMgcSML59aejzg9eucT+5tWL2rvnUobPhWJoyiQ5uRhJxmEkY/uYxR3n+kTEVL01LZbTZGkOkB6bcHwgRKYDCTZfHM2HjvRn/A8ujCsfnE+ZvwESfffsOB/NeRhIWpwfyfuvHB/LHxuAI4LaraPQ44Nph5wdyem4LFJVgwwTU1V8AexTsFoCWj8+DyI5R6ain5TjyTlu9HNVHne2TuHJsoZ9Z/B3rY34+Vk6jaUod7VYqrI8vYAO6EAFhfBsFWWcK+np88u8np6e5RqeJQv3hC0ljZ3vw18pQszTi0TM5W1keiSam32fLajdqeu+8Oe4zQLp4fDmVhK452NkFg9kkc8JX9sWYttXRClZ/EXqN2Ao7bpvnB7zXvh4iP/q6Kj6wcVxDQhDVwBIeB4QnQ2FxAXltssLo5dRS3VWxC1n96om/ws7WslDW1vF+vawTyll6bwnXF9owC9mzmUekM6YoSqkPWoYa1vDJBHRWcZl/NKozd85m3KPXc24QHoML35lwlJVZeGrFkF5eq+fTLI3TictuAbV0K47JDLfL8F+QYoRWcgf37Ni1mP9/Tv9058gvw82EFhihiEUbBQ/C/b7k0BRTge+9xZYCMwOjoX7/x813ix2cH47+Bu///dl+9jBeb4fvH8sMDv4PDHteNiYvxvsb9/EcleDpSrLW8GxQ2UdwltBPVWzXutgWTlD8/j+z8jUnHIPmVpr9t1Z2tZyq6cDZW31YFn7q+dw5/eDeih19j2zdOY/C67p8WBbiRymX3OiyvIcK/sNZ4qH2wj32azt7qXv3Tlnu6MP/cX7pddPQNf7v0CP2xGMexaCLDMh/Ee2t9v/6sHVtLPJiCwh34msw71/fO+q86OPr6n9SccA0tLQxQSHAYF7OBAQx4zjuqYIS1fYiiZTrGsP89UtluhptgiQtbqqNaSbGlVdn/unh3L+hxfGxeHLGXE5mddHM57dZKnp1qgO3KNEBCeqwzg3dYVv6AiJnrgFxEqV41ezvsMY+UZfj7VrVdQorKlbwHV92p/J/82rl9ibp5PRREQr5BUsc2K5Cva/g/1XsFyhdXxvD5GQkJCQWDjKnVPeIcV1Ye0k8ORAL86sK9QT/Rn14MVx70s72jlZoqj/GAnm6NUJ56VPR9Xzw3kzYqoFRaSrKkM11hIxeNxSWTyikXhYJ/GQxrd1R8mGzogaD2t6cG2T7ASsZtzWE9W3dke91+Jj+RePD/vAXqBcRbwtarAJm7mDE47REta1nStj9N6Ncbp9RcwEflMurMl7//nNq/ZrJ8a8Na2m1ho15l0HIEzZmWs5fvpaTjNATms3EughsE9KhCchISEhsTik95EorvjfBRZB5aGrmNWckv4xR/vw/Dj7/OZW1zKUJVm0Z3ucv302SQfGHR1UnEooFVFLZ59Z1+ze3RsXKxImBeKjMVNVQoaqofrCONRkWuaI6coWdwNS5DFTsxNhJtKOr4HKE6tadPqZdU30rnVx2tsRNkE9Tg7l9raHtb41zdqbZ5LuqcGsu6cXxOP8hjn5cNp1QemJ0YyrhQ21GHBTTBYYsyhh2LGLsmlKSEhILC7poZs8Bjj+InTAGwsMQYtEknG4dnIwy471p93b18YNuvhqT9g+F1eStpJ1fOL4nHeENP+RbW3+N/d0KataLL2M2GrKL+sF7ptjWV89PpDVVrZYfH1HSHlwa6u2ojWk4uoG4PnS9RWSqCMn7d3QrJ8YzPhvnE75q1pD2sqEVfMSDi5EIXD2hxfSChxTK83llXkIIdl9IFWehISExOKTHuLjwHAtRmGhuqIoxACKGUg5yusnk2JrT8wHZWUsdsF0lYjuZpM1hwwRB9r52u5O8s093Vp33DDIPBbSIbE4wHinBnPeK8dHvbzni4e2tYr7NyW0bSuiqqoq4sDxJLuUdPnd65qVLR2W6nAuIirVdIUqnU2mCvvpOMR5+lrO64njGvaa1J4YnvC89y6k6MXRnBk1FFSd5XN5+OpVWgwybcumKSEhIbH4pHcc7F2wB8E6ikllCQ5x0qzNtPfOp7XPbp6wd69qVkEBLqonZ0hX9d+9vZOgB6YOhbgNCKctputknivH8y7jb5wec946lfISYY3+0d09+o4VMbUprOlpQfiPXz3jn/uHH4Gs4+oLX/kai63u4XRsjH5hS4JuX91cWO9+19om9fS1rPf++XG2tjXkr2sP1RKb0z9yJe0dvjyhuUzoCQwhM7UiHf8dAXuZFB1ZJCQkJCQWg/TojYrofbBj8H5HYdxQFCOZqDpVBtK28fLxsdz69ojXEtEVsnihSyjKqHXtYWNtW6hAzPDnQkhWDI473lunU27UUpXfu7PLXBG3DE0tFt+nhF/jOo8MXTU2XTmuXeC2n6E69/I2fev3H2XNXXeSVQbRO5ssfefKJvazQ8Pu6cEskh6WqZqhXj447rpvnx6nV1O2EbU0Up4Elxbn8l6Hlx/ickPZLCUkJCQWBzN12Din9E+kuPaClNINmapKXE9ob5xKGkcuT4BuEouefQHDxQD54do4daGr4/KuEFnbJ1u6Itqa1pBeIjzBiegfFzyvGtRqS5CdI2fI7x/4z9rdB1/UI9lx9WLSEyMZUVgAiGW4fVVU722zlCP9GXbqWraqOmCce2+fSfofXEzpPhNa2LghuDQG/P4xKWZJF7JZSkhISCwd6aHSeAN63sNiSv3hHBsxNEpHMp7xwqFr9NKYjfvxZXKdtCmsqu0xQ7mccvyxrMdK0U+SgvgvHxnyUi++rHaeOqTFs2nSkk2Ric4VZPRL3/C27dlK1oT45NK81pip3rsxoSXhGCcGMn4VJMVPXct5v/50RBlMu6jypkez9kRhLo++DeZQcuN/EhISEhJ1Ir1SutVpaVdxbu+nYNcm38PsC7qCwajVD86njQPHRvx03nOXizJpjxrqnb3N2sXRPEZa8YsDt8UEsgZzhXXpLOn3dP7q1vv5ofYNxM67XI3H2ZYNcaXFmgq2jRSEKfyyDhOgfCuSfjrvu1BX/NOBjIlDtCbU4bQKw5AqfydVnoSEhMTNUXoIVBwvgk2G5yn17hZ02rbLjRc+HlLfPz/uCTEVnLmRAeVWdq5s0lvDuvLRxbR7LWUXyh2jRHtwQ7PR+vDn2eXv/Cv7o2//a+flvt/xLypRQsZTSrSQcaJAkCKV89mrJ0btXx4e8hIRnW7qily3AH46GBfuy8dG3ZeOjum2y/SIeUP+IPTSfBHsN6A8PUFm/k9CQkJCoj7Q5uizz4P9ED7dCdsdhV4fnVpA6kUtlV4ay1s//ngo19MccretiCB5qo1+sS0RQ927PqH99NCQ++7Zce+LOw1UXsqm7pjhPrrXS6kaT6Zccrgp6vHREeXze9aqq01Cx/N+YSjznbPj7NJojrXHDPrQtjYLSG+upRuY68/+8SdDSn/KMcKmSjGIN7uew44FKi8lm6KEhITEkpDezMDAzbB5nRZjQK5FUVT6TNcoiZma+t65tNUaGbS/Y/Y4a9tCFlmiEGXzvliF0F2rY8ZHl9Lsw4tptntNk7+mNYSxNOmu1kLoMpKP6mT9F3d7TFfERriic1fH+Rvnxv2BlONrKqV71sb1u9fHtd720Fwqj58cyNr/5b0BenwwY1mmUhjWnJam6ArY/0eKAQEkJCQkJJYAau8X9s/1uUuLkaxXgW0rvYk9PWY4yHtCvTCSx5lBt7c9TKKmNudwXyMAhzmRez4dyPqwFUheetFNpWCgxmhniNJMOidePz7q/+bkmDeW8zlcn/7o7R36/ZtbzESkkKd9VsI7P5y3/+6tq/zVU0lLUageMYtr8so4D4c1fwj21yTIpDAX5sqyICEhISFRB6VXBnRq+Y/QZ6+H7e5y4kuEVTI04Zk/OzzMY5bmPdbXRYEQzIYmPmCe3atj6rH+jPr+uXG+c0WMbekuDM+SrMP4xVGbnRiY8I5cTvvjNiNbu6Pq57e16qtbLCTHSkoWUxI5P3xvwHvl5GhYEK5HTb1QGby09qMwPSreIMVhzTHZBCUkJCSWkPRoZUcJHOY8AHu1oDKBbrs14I5CH94S1cjIhGM9/8EgqCjV+b3bO2nUUucVKmxJQAvRXpS71jXrpwaz9punx1xUepahkEOXJrw3z6T88Zwrdq2Kqt/a2qatarEMU6sqlxC/Mma7z3844P388JDlcKE3hXRyfaixwosTYP+ByGFNCQkJiYZUegjM4I1ehv8euu1/SYrzewW+xEwMLWGDjoDi+6cPr/Gwrjhf3tmOBGjQxl1kRretiGr3b0qoB46P+RfHHFy3R/PAVJs6wsrePZ3G+o4IeltWNUfJheD9Sdt+7v1B74VPhi04mtkc1gqEx8VURGx4OYyqGexHsulJSEhINC7pIUbBfgCGw5eYcbul0JFzeENXSJwS5cq4HfpPb121R7Ku/bu7O3lHkxmiDUp7KqXK57a2mhFLoxiEGjZ0Y1fY2NYd04Gwqg6xBipOnBnK2//wzlXn5eOjYccXZpOl4nrG8iFN3BPn7v4W7B+DhwgJCQkJiQYmPezCBwKlsgHsd1DoFYhPUGJoGokQpgylndCPDg6rE7bv/OHdPXxl3MKU8g3p1Rk2VO2+jQnltp6oYWkKjViqUmPmBP/0taz9d+9cFS8fT4YZB8ILFB4rG9IMXv6EFL01r8lmJyEhIdH4pIfA+T2ck/pLMFyi8LnSMTA4pQkCSVE0msy7xk8/GaHprG9//Y6uib61TVagEBtP8SlUacM8PzUqUgwM/cGFceeH7w6Qd84kTQ68HwuVK7zr8AuwfxfUnVxtLiEhIbFMSK9EfG+DPYU+IbD9bEnLoMMG+nxEDZXmXa6/cjJJhjO+PZbpsO/ZGBcRUzMaUfXVSHh8PM+c106Ouj85NKQevZzBzEtacwjDi9FJwps6JMV0QX8FdfRBUHcSEhISEsuI9BCFoNSB4sMIZfdPKiAkPpA7EUultsf0jy+llZGM65wayrkPbm5hmBcPdpt3XrybCOEz4Z0YzHoHjo+wV4+NGQNpx9A1RQ3rStFR5UYN9yuw/wvsLSLn8SQkJCSWLemRoBP/JSkuXv8u2FdLKo4HaXhCukoVwrULo3nlH9/r9z69OuF+aUebvWddnHU2m/oCz79kZAdkxq5NuJiPz3vx02HlWP+E5bhcw1gu6MRTSohXxuJpURzS/D4tpmryZVOTkJCQWN6kh8DIIq/RYqeO/f7vwDZcUnxIArj+DefNJhzf/PBCWr2acrxDlzP+A5tbvG0rokZLZDIbeqMNe3LMGTia9djhyxPeu+dS4v3z43p/ysFE7lo0pBWWa+B1ijLCg+0Q/Pu3goi/hz+PyCYmISEhceuQXglvkmIi1LNgfwTWHaxLKxACxups0QyS97h2Oemo19LD3uErGb9vbcy7Z0PCua0nqjWHNbMsO/rNGvosDFAyTvzBtOMcvDjufXwprQLpaUB2mse4HjE0ipkmOOw6g8PKUTK1LKFfNi8JCQmJW5P0EIeBqZ4SBaVDvgO2hQSZF4oLtAWxdEpMTaMZhxmfDkzoF0Zz7tGrGef21U3+pu4o2dAeUlYkLBoxJhe2LxX5ITfzibznXRlz+NnhPD90Jc0/AGV6OZk3fF+YEVOlzWE98M68IeFPThSJ/z9AsXFpgiObloSEhMStTXqIMej0cQH7SaCF/wG2D4J1FSXUVDyumKWSCOg62+UmkJ5xfCDrdTRZ/pauMLutJyLWd4RZR0wn7U0mbQ5puq4qpQDP9SDByZy5rs9ZMu+z0bSLc3b01GCWH706Qc4O59ThjGtxTjQMFm1FlMn4mTd6Z5JLophwF4czZWgxCQkJid8i0kPkSZEEDoL9Kdg3gCA2AQlGymkHaSRkqLi2j7qMG4PpvHE1mRNvnRn1u5ste01ryN3UFSbr28Osq9nSgChpSFdUHFo0dZVgvExdvW6xQVm0r0lyK/zjMyEcxoXjceJ4jGcdxjIOFwPjeXZmKO+fuZYl54bzBhCf5fpMw+OGjcI5JtczTPfMhD8zsDkGn/4t2D/A3+OyOUlISEj89pFeCTin9VfAGm8D4X07UH2YI8co8QdGLlFUSjQQcpiqCHiJgun9KVu9knTC718Y502m5iWiGutqMklHzPRbYwZpj+qiHZVgzFRKxIeOokZRERJUcB4XhWTvji/E8ITLhyYcMjLhkaG0SwbTNhnLenQ04+lZl4WABzGlHjU0IDtTJ7DBMGVEBOJ0GuHh0CUO4f4cVO0/wPYd2EOuv5OQkJD4LSc9RA7sVbCjwB/3i2LMzr1gcfhbBTKhJT5BklG1ojckkJfic0E8IdRU3tOSOU+cG7YL82maSgUoMAaqj8XQgxLIEr4rogblMUsvDFvCd2jW4RSP5TJBJmxPsz2meoyoQIaEcwZKTaFAuhS1HBwLiBejydDJheqFeUhRLuwKC8tRzb1Ois4quE4xKZuQhISEhCS96cDsAi+QonfjfWBfFsUF7ZiuaGpMMiAZzOmKC9wxdhkSIxAQRRJkIN4KZMiYmnWYGMl4xZDOyKAozALG4gIVI7mOseCTAsnhsU1DJZgaD5VmaVH55JjozEHCkgHZ/SwguwtELjaXkJCQkKQ3B1ApnQa7SIqejneA7QN7BKynfMdyAsQNkpMJKpATdVJ9lciw5EdZcDIJvEw0VSFGcGWFBYAl+UaDKToxtZyi7FQz4URAdmiHwM6T4vAml01HQkJCQpJeJSBZ4IL2U4FaehfE12tIgCDUtsH2NhEQ4HQiEmKa6yYtkmHp3UIuIGWK3KaDTlNydHayOwd2BMp1Agj1fdh+BNtBUgy9JslOQkJCQpLevMkPCQYJBWNUrgLrA9sOthlsNdhKsOgNKpDMQmpVLWig04+UDtTnJbAzYB+CfUSKziqZoJwSEhISEpL06oJcYEgyJwOSawdq2oYJzkFlrYW/O8DipJixHaf6msFayY3ib0ZSDLL9jMDLFC0OT46LItmNwvYsqLlDcB48N87dobNKXjYNCQkJCUl6i41MYIMBAb4CFhFFglsVGA5/7gB7CAmQzqIAp4k+HJp8F977GMgNk7ieDUg2HRiSnVx2ICEhISFJ76YBiWo4MCSoVEBOY6QY3gyHP9cEqmy2gU18HwNaXwE7Roreo5j9/XxwHIfIpK4SEhISvzX4/wUYAF99gm7cUGR7AAAAAElFTkSuQmCC',
          fit: [100, 100]
          },
        { text: 'Invoice Order Jasa Antar Obat', style: 'header center' },
        { text: { text: this.letterObj.orderid }, style: 'subheader' },
        { text: 'Status Order', style: 'subheader'},
        { text: { text: this.letterObj.status }, alignment: 'left' },
        { text: 'Detail Produk Pesanan', style: 'subheader' },
        {
          table: {
          // headers are automatically repeated if the table spans over multiple pages
          // you can declare how many rows should be treated as headers
          headerRows: 1,
          widths: [ '*', 'auto', 100, 'auto','*' ],
  
          body: [
            [ 'Nama Produk', 'Tgl Lahir', 'Penanggung jawab', 'Keterangan', 'Harga' ],
            [ { text: this.letterObj.nama }, { text: this.letterObj.tgl }, { text: this.letterObj.penanggungjawab },{text:this.letterObj.keterangan}, { text: this.letterObj.price } ],
           ]
          }
        },
        { text: 'Tanggal Order', style: 'subheader' },
        { text: this.letterObj.date },
        { text: new Date().toTimeString() },
        { text: 'Informasi Pasien', style: 'subheader' }, 
        { text: this.letterObj.namapasien},
        { text: this.letterObj.emailpasien},
        { text: 'Catatan', style: 'subheader' }, 
        { text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20] },
 

      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        nama: {
          fontSize: 12,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }
 
  downloadPdf() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }
 


}
