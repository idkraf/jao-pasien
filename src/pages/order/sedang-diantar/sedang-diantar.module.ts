import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SedangDiantarPage } from './sedang-diantar';

@NgModule({
  declarations: [
    SedangDiantarPage,
  ],
  imports: [
    IonicPageModule.forChild(SedangDiantarPage),
  ],
})
export class SedangDiantarPageModule {}
