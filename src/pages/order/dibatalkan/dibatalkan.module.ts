import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DibatalkanPage } from './dibatalkan';

@NgModule({
  declarations: [
    DibatalkanPage,
  ],
  imports: [
    IonicPageModule.forChild(DibatalkanPage),
  ],
})
export class DibatalkanPageModule {}
