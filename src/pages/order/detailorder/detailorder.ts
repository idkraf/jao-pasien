import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';

import { CommonServicesProvider } from '../../../providers/common-services/common-services';

/**
 * Generated class for the DetailorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailorder',
  templateUrl: 'detailorder.html',
})
export class DetailorderPage {
  order: any = {};

  pdfObj = null;
  
  constructor(
    public values:ValuesProvider,  
    public service: ServicesProvider,
    public commonService:CommonServicesProvider,
    public navCtrl: NavController, public navParams: NavParams) {

    this.order = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailorderPage');
  }

  ionViewWillEnter() {
    this.commonService.dismissLoading(true);
  }

  getOrderPdf(id){
    this.navCtrl.push('ViewpdfPage', {id: id});
  }

  getPrintPdf(id){

  }
}
