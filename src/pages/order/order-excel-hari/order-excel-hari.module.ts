import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderExcelHariPage } from './order-excel-hari';

@NgModule({
  declarations: [
    OrderExcelHariPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderExcelHariPage),
  ],
})
export class OrderExcelHariPageModule {}
