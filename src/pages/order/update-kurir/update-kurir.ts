import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';
import firebase from 'firebase';
/**
 * Generated class for the UpdateKurirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-kurir',
  templateUrl: 'update-kurir.html',
})
export class UpdateKurirPage {
	form:any = {};
  errorMessage: any;
  orderDetails : any;
  kurirList: any;
  kurir:any;
  disableSubmit: boolean = false;

  constructor(
    public navCtrl: NavController, 
    public params: NavParams, 
    public values:ValuesProvider,  
    public service: ServicesProvider,
  ) {

    this.form  = params.data; 
    
    this.service.getOrderDetail(this.params.get('id')).on('value', (snapshot) => {
		  this.orderDetails = snapshot.val();
		});

    this.service.getKurir().on('value', snapshot =>{

  		this.kurirList = [];

  		snapshot.forEach(snap =>{
  			this.kurirList.push({
  				id: snap.key,
	  			name: snap.val().name,
          phone: snap.val().phone
  			});
  		});
    });	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateKurirPage');
  }

  updateOrderKurir(form, id){
     if(this.validate()){
         //   this.service.addOrderKurir(this.form.kurir, this.form.id)
      //.then( () =>{
      //  this.navCtrl.pop();
     // });
     }    
  }

  validate(){
    if(this.form.kurir == undefined || this.form.kurir == ''){
     this.errorMessage = 'Mohon Pilih Kurir';
     return false;
   }  
   return true;
 }
}
