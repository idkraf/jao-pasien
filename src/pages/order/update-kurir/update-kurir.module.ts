import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateKurirPage } from './update-kurir';

@NgModule({
  declarations: [
    UpdateKurirPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateKurirPage),
  ],
})
export class UpdateKurirPageModule {}
