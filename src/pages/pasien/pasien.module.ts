import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PasienPage } from './pasien';

@NgModule({
  declarations: [
    PasienPage,
  ],
  imports: [
    IonicPageModule.forChild(PasienPage),
  ],
})
export class PasienPageModule {}
