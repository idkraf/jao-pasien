import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import {ValuesProvider} from '../../../providers/values/values';
import firebase from 'firebase';
/**
 * Generated class for the DetailpasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailpasien',
  templateUrl: 'detailpasien.html',
})
export class DetailpasienPage {

  pasien: any = {};
  pasienID:any;
  currentUser: any;
  errorMessage: any;
  invoiceList:any;

  //public verifikasiRef:firebase.database.Reference;

  constructor(
    public modalCtrl:ModalController,
    public nav: NavController, 
    public params: NavParams, 
    private alertCtrl: AlertController, 
    public service: ServicesProvider, 
    public values:ValuesProvider) {

    this.pasien = params.data;
    this.pasienID = this.params.get('id');

    this.currentUser = firebase.auth().currentUser;

   // console.log(this.pasien);

  	this.service.getPasienOrderList(this.params.get('id')).on('value', snapshot =>{
    	this.invoiceList = [];
		  snapshot.forEach( snap => {
      	this.invoiceList.push(snap.val());
    	  });
      });
  }

  lihatOrder(order){
    this.nav.push('DetailorderPage', order);
  }
  
  edit(pasien){
    let modal = this.modalCtrl.create('EditpasienPage', pasien)
    modal.present();
  }
  gantiGambarPasien(item){
    let modal = this.modalCtrl.create('EditfotopasienPage', item)
    modal.present();
  }
}
