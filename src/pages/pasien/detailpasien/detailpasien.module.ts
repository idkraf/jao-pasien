import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailpasienPage } from './detailpasien';

@NgModule({
  declarations: [
    DetailpasienPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailpasienPage),
  ],
})
export class DetailpasienPageModule {}
