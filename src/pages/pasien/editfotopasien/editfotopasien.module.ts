import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditfotopasienPage } from './editfotopasien';

@NgModule({
  declarations: [
    EditfotopasienPage,
  ],
  imports: [
    IonicPageModule.forChild(EditfotopasienPage),
  ],
})
export class EditfotopasienPageModule {}
