import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import firebase from 'firebase';
import { ServicesProvider } from '../../../providers/services/services';
/**
 * Generated class for the EditfotopasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editfotopasien',
  templateUrl: 'editfotopasien.html',
})
export class EditfotopasienPage {
  disableSubmit: boolean = false;
  pasien: any = {};
  fileName: any;
  storageRef: any;
  uploadTask: any;
  downloadURL: any;
  selectedFile: any;
  errorMessage: any;
  constructor(public service: ServicesProvider, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {

    this.pasien = navParams.data; 

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GantigambarrsPage');
  }

  onChange(event){
    this.selectedFile = event.target.files[0];
    this.disableSubmit = true;
    console.log(this.selectedFile);
    this.upLoad();
  }
  
  validate(){
    if(this.pasien.downloadURL == undefined || this.pasien.downloadURL == ''){
      this.errorMessage = 'Please Add Image';
      return false;
    }
   
    return true;
  }
  
  upLoad(){
  
    var fileName = this.selectedFile.name;
  
    var storageRef = firebase.storage().ref('pasien/' + fileName);
  
    var metadata = {contentType: 'image/jpeg'};
  
    var uploadTask = storageRef.put(this.selectedFile, metadata);
  
    uploadTask.on('state_changed', (snapshot) =>{
      console.log(snapshot);
  
      var progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
        console.log('upload' + progress + '% done');
  
      switch(uploadTask.snapshot.state){
        case firebase.storage.TaskState.PAUSED: 
          console.log('upload is paused');
          break;
  
        case firebase.storage.TaskState.RUNNING:
          console.log('upload is running');
          break;  
      }
  
      }, (error) =>{
          console.log(error);
        }, () =>{
  
          this.pasien.downloadURL = uploadTask.snapshot.downloadURL;
          this.disableSubmit = false;
          console.log(this.pasien.downloadURL);
          console.log("successfully uploaded");
    });
  }
  
  dismiss(){
    this.service.editgambarpasien(this.pasien.id, this.pasien.downloadURL)
    .then(() =>{
      this.viewCtrl.dismiss();
     }).catch( (error) => {this.handleErrors(error);
      });
    
  }
  handleErrors(error){
    this.errorMessage = error.message;
    console.log(error);
  }
}
