import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import {ValuesProvider} from '../../../providers/values/values';
import firebase from 'firebase';

/**
 * Generated class for the EditpasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editpasien',
  templateUrl: 'editpasien.html',
})
export class EditpasienPage {
  pasien: any = {};
  errorMessage: any;
  disableSubmit: boolean = false;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public service: ServicesProvider, public values:ValuesProvider) {
    this.pasien = navParams.data;  
  }
savePasien(){
  if(this.validate()){
  this.service.editPasien(this.pasien.name, this.pasien.phoneNo, this.pasien.keterangan, this.pasien.id)
  .then(() =>{
    this.viewCtrl.dismiss();
   }).catch( (error) => {this.handleErrors(error);
    });
  }
}
validate(){
  if(this.pasien.name == undefined || this.pasien.name == ''){
   this.errorMessage = 'Silahkan isi nama';
   return false;
 }

 if(this.pasien.keterangan == undefined || this.pasien.keterangan == ''){
   this.errorMessage = 'Mohon isi keterangan';
   return false;
 }

 if(this.pasien.phoneNo == undefined || this.pasien.phoneNo == ''){
   this.errorMessage = 'Silahkan isi telepon kurir';
   return false;
 }
 return true;
}
handleErrors(error){
  this.errorMessage = error.message;
  console.log(error);
}
dismiss(){
  this.viewCtrl.dismiss();
}
}
