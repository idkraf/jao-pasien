import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditpasienPage } from './editpasien';

@NgModule({
  declarations: [
    EditpasienPage,
  ],
  imports: [
    IonicPageModule.forChild(EditpasienPage),
  ],
})
export class EditpasienPageModule {}
