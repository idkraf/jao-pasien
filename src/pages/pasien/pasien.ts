import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {ServicesProvider} from '../../providers/services/services';
import firebase from 'firebase';
/**
 * Generated class for the PasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pasien',
  templateUrl: 'pasien.html',
})
export class PasienPage {


  currentUser: any;
  user: any;
  pasienRole: any;

  public pasienList:Array<any>;
  public loadedpasienList:Array<any>;
  public pasienRef:firebase.database.Reference;

  public verifikasiRef:firebase.database.Reference;

  constructor(public modalCtrl:ModalController,public nav: NavController, public navParams: NavParams,  
      private alertCtrl: AlertController, public service: ServicesProvider) {

    this.currentUser = firebase.auth().currentUser;
    this.user =firebase.auth().currentUser;
    this.pasienRole = {};

    this.pasienRef = firebase.database().ref('/pasien');
    this.pasienRef.on('value', pasienList => {
      let items = [];
      pasienList.forEach( item => {
        items.push(item.val());
        return false;
      });
    
      this.pasienList = items;
      this.loadedpasienList = items;
    });

    this.initializeItems();
  }

  initializeItems(): void {
    this.pasienList = this.loadedpasienList;
  }
  
  cariPasien(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.pasienList = this.pasienList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.pasienList.length);
  
  }

  detailPasien(item){
    this.nav.push('DetailpasienPage', item);
  }
  editPasien(item){
    let modal = this.modalCtrl.create('EditpasienPage', item)
    modal.present();
  }


}
