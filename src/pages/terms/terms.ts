import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{ ServicesProvider } from '../../providers/services/services';
/**
 * Generated class for the TermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html',
})
export class TermsPage {

  title:any;
  keterangan:any;
  form: any;

  constructor(
    public service: ServicesProvider,
    public navCtrl: NavController, public navParams: NavParams) {
    
    this.title={};
    this.keterangan={}
    this.form={}

    this.service.getTerms().on('value', snapshot =>{
      this.title = snapshot.val().name;
      this.keterangan = snapshot.val().description;
    });
  }

  saveTerms(){
    this.service.addTerms(this.title, this.keterangan)
       .then(() =>{
         console.log('saved');
         this.navCtrl.pop();
       });
 }
}
