import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddlokasirsPage } from './addlokasirs';

@NgModule({
  declarations: [
    AddlokasirsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddlokasirsPage),
  ],
})
export class AddlokasirsPageModule {}
