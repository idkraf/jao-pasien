import { Component,ViewChild ,ElementRef } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { CommonServicesProvider } from '../../../providers/common-services/common-services';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation';
import { Location } from '../../../models/location';
import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
/**
 * Generated class for the AddlokasirsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
@IonicPage()
@Component({
  selector: 'page-addlokasirs',
  templateUrl: 'addlokasirs.html',
})
export class AddlokasirsPage {
  @ViewChild('map') mapElement: ElementRef;
  public map: any;
  public markers =[];
  public location:Location;
  public lat : number;
  public lng : number;
  public placelabel:string;
  rmsakit:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public platform:Platform,
    public angularFireDatabase: AngularFireDatabase,
    public commonService:CommonServicesProvider,
    public viewCtrl: ViewController,
    public modalCtrl:ModalController,
    private geolocation : Geolocation,
  ) {
    this.rmsakit = navParams.data;
    platform.ready().then(() => {
      this.loadMap();
    });
    
  }
  ionViewWillEnter() {    
    this.commonService.dismissLoading(true);
  }
  
  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
        let mapOptions = {
          center: latLng,
          zoom: 16,
          myLocationButton:true,
          compass:true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      google.maps.event.addListener(this.map, 'click', (event) => {
       this.setMapOnAll(null);
        var location  = event.latLng;
        this.lat = location.lat();
        this.lng = location.lng();
        console.log(this.lat);
        console.log(this.lng);
        let image = 'assets/imgs/blue-bike.png';
        this.addMarker(location);
      });
      this.addMarker(this.map.getCenter());
    
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  addMarker(LatLng){
    let marker  = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: LatLng
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  sendLocation(){

  this.location = new Location (this.lat,this.lng,this.placelabel);
  console.log(this.location.lat,this.location.lng,this.location.label);

  this.angularFireDatabase.object('/rs/'+this.rmsakit.id+'/').update({
      lat: this.location.lat,
      lng: this.location.lng,
      alamat: this.location.label,
      setlokasi: true
  });
 // this.commonService.presentLoading('Berhasil rubah lokasi Toko');
//  this.navCtrl.setRoot('TabsPage');
this.navCtrl.pop();
}

  
close(){
this.viewCtrl.dismiss(); 
}

}
