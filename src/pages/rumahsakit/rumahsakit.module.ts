import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RumahsakitPage } from './rumahsakit';

@NgModule({
  declarations: [
    RumahsakitPage,
  ],
  imports: [
    IonicPageModule.forChild(RumahsakitPage),
  ],
})
export class RumahsakitPageModule {}
