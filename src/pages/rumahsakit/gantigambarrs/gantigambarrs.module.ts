import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GantigambarrsPage } from './gantigambarrs';

@NgModule({
  declarations: [
    GantigambarrsPage,
  ],
  imports: [
    IonicPageModule.forChild(GantigambarrsPage),
  ],
})
export class GantigambarrsPageModule {}
