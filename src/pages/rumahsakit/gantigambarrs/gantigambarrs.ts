import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import firebase from 'firebase';
import { ServicesProvider } from '../../../providers/services/services';
/**
 * Generated class for the GantigambarrsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gantigambarrs',
  templateUrl: 'gantigambarrs.html',
})
export class GantigambarrsPage {
  disableSubmit: boolean = false;
  rm: any = {};
  fileName: any;
  storageRef: any;
  uploadTask: any;
  downloadURL: any;
  selectedFile: any;
  errorMessage: any;
  constructor(public service: ServicesProvider, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {

    this.rm = navParams.data; 

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GantigambarrsPage');
  }

  onChange(event){
    this.selectedFile = event.target.files[0];
    this.disableSubmit = true;
    console.log(this.selectedFile);
    this.upLoad();
  }
  
  validate(){
    if(this.rm.downloadURL == undefined || this.rm.downloadURL == ''){
      this.errorMessage = 'Please Add Image';
      return false;
    }
   
    return true;
  }
  
  upLoad(){
  
    var fileName = this.selectedFile.name;
  
    var storageRef = firebase.storage().ref('rumah-sakit/' + fileName);
  
    var metadata = {contentType: 'image/jpeg'};
  
    var uploadTask = storageRef.put(this.selectedFile, metadata);
  
    uploadTask.on('state_changed', (snapshot) =>{
      console.log(snapshot);
  
      var progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
        console.log('upload' + progress + '% done');
  
      switch(uploadTask.snapshot.state){
        case firebase.storage.TaskState.PAUSED: 
          console.log('upload is paused');
          break;
  
        case firebase.storage.TaskState.RUNNING:
          console.log('upload is running');
          break;  
      }
  
      }, (error) =>{
          console.log(error);
        }, () =>{
  
          this.rm.downloadURL = uploadTask.snapshot.downloadURL;
          this.disableSubmit = false;
          console.log(this.rm.downloadURL);
          console.log("successfully uploaded");
    });
  }
  
  dismiss(){
    this.service.editgambarrs(this.rm.id, this.rm.downloadURL)
    .then(() =>{
      this.viewCtrl.dismiss();
     }).catch( (error) => {this.handleErrors(error);
      });
    
  }
  handleErrors(error){
    this.errorMessage = error.message;
    console.log(error);
  }
}
