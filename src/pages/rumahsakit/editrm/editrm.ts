import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import firebase from 'firebase';
/**
 * Generated class for the EditrmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editrm',
  templateUrl: 'editrm.html',
})
export class EditrmPage {
  rmsakit:any;

  fileName: any;
  storageRef: any;
  uploadTask: any;
  downloadURL: any;
  selectedFile: any;

  errorMessage: any;
  disableSubmit: boolean = false;

  constructor(
    public service: ServicesProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
    this.rmsakit = navParams.data;
    console.log(this.rmsakit);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditrmPage');
  }

  editRS(){
    if(this.validate()){
    this.service.editRm(this.rmsakit.downloadURL, this.rmsakit.name, this.rmsakit.description, this.rmsakit.teleponcc, this.rmsakit.teleponapotik, this.rmsakit.openclose, this.rmsakit.id)
       .then(() =>{
         console.log('saved');
         this.viewCtrl.dismiss();
       });
      }
  }

  onChange(event){
    this.selectedFile = event.target.files[0];
    this.disableSubmit = true;
    console.log(this.selectedFile);
    this.upLoad();
  }


  upLoad(){
    var fileName = this.selectedFile.name;
    var storageRef = firebase.storage().ref('rumah-sakit/' + fileName);
    var metadata = {contentType: 'image/jpeg'};
    var uploadTask = storageRef.put(this.selectedFile, metadata);
    uploadTask.on('state_changed', (snapshot) =>{
      console.log(snapshot);
      var progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
        console.log('upload' + progress + '% done');
      switch(uploadTask.snapshot.state){
        case firebase.storage.TaskState.PAUSED: 
          console.log('upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING:
          console.log('upload is running');
          break;  
      }
      }, (error) =>{
          console.log(error);
        }, () =>{
          this.rmsakit.downloadURL = uploadTask.snapshot.downloadURL;
          this.disableSubmit = false;
          console.log(this.rmsakit.downloadURL);
          console.log(this.downloadURL);
          console.log("successfully uploaded");
    });
    }



  validate(){
    if(this.rmsakit.name == undefined || this.rmsakit.name == ''){
     this.errorMessage = 'Silahkan isi nama produk';
     return false;
   }

   if(this.rmsakit.description == undefined || this.rmsakit.description == ''){
     this.errorMessage = 'Mohon keterangan rumah sakit';
     return false;
   }

   if(this.rmsakit.teleponcc == undefined || this.rmsakit.teleponcc == ''){
     this.errorMessage = 'Silahkan isi telepon rumah sakit';
     return false;
   }

   if(this.rmsakit.teleponapotik == undefined || this.rmsakit.teleponapotik == ''){
     this.errorMessage = 'Silahkan isi telepon apotik rumah sakit';
     return false;
   }

   if(this.rmsakit.openclose == undefined || this.rmsakit.openclose == ''){
     this.errorMessage = 'Silahkan isi Waktu buka dan tutup rumah sakit';
     return false;
   }

  
   return true;
 }

    dismiss(){
      this.viewCtrl.dismiss();
    }
}
