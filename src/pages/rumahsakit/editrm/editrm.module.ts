import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditrmPage } from './editrm';

@NgModule({
  declarations: [
    EditrmPage,
  ],
  imports: [
    IonicPageModule.forChild(EditrmPage),
  ],
})
export class EditrmPageModule {}
