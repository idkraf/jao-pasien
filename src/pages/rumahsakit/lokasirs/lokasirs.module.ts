import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LokasirsPage } from './lokasirs';

@NgModule({
  declarations: [
    LokasirsPage,
  ],
  imports: [
    IonicPageModule.forChild(LokasirsPage),
  ],
})
export class LokasirsPageModule {}
