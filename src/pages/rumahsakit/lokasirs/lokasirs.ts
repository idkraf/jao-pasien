import { Component, ViewChild, ElementRef } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { CommonServicesProvider } from '../../../providers/common-services/common-services';
declare var google;
/**
 * Generated class for the LokasirsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lokasirs',
  templateUrl: 'lokasirs.html',
})
export class LokasirsPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  rmsakit:any;

  constructor(
    public platform:Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public commonService:CommonServicesProvider,
    public modalCtrl: ModalController
  ) {
    this.rmsakit = navParams.data;
    platform.ready().then(() => {
      this.loadMap();
    });
  }

  ionViewWillEnter() {
    this.commonService.dismissLoading(true);
  }

  loadMap(){
        let latLng = new google.maps.LatLng(this.rmsakit.lat, this.rmsakit.lng);
        let mapOptions = {
          center: latLng,
          zoom: 16,
          disableDefaultUI: true,
        //  zoomControl: false,
        //  draggable: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.addMarker(this.map);
    
      }
    
      addMarker(map:any){
    
        let marker = new google.maps.Marker({
          map: map,
          animation: google.maps.Animation.DROP,
          position: map.getCenter()
        });
        let content = "<h4>{{this.rmsakit.name}}</h4>";        
        this.addInfoWindow(marker, content);
      }

      addInfoWindow(marker, content){

        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
      }

      lokasiPage(item){
         let modal = this.modalCtrl.create('AddlokasirsPage', item);
         modal.present();
         modal.onDidDismiss((res)=>{
         });
       }
}
