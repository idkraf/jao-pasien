import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';
/**
 * Generated class for the DetailrmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailrm',
  templateUrl: 'detailrm.html',
})
export class DetailrmPage {

  rmsakit:any;
  invoiceList:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl:ModalController,
    public service: ServicesProvider,
    public values:ValuesProvider
  ) {

    this.rmsakit = navParams.data;
    console.log(this.rmsakit);
    this.service.getOrderListRM(this.rmsakit.id).on('value', snapshot =>{
    	this.invoiceList = [];
		  snapshot.forEach( snap => {
      	this.invoiceList.push(snap.val());
    	  });
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailrmPage');
  }

  lihatOrder(order){
    this.navCtrl.push('DetailorderPage', order);   
  }

  gantiGambarRM(item){
    let modal = this.modalCtrl.create('GantigambarrsPage', item)
    modal.present();
  }

  lokasiRM(item){
    this.navCtrl.push('LokasirsPage', item);
  }

  createlokasiRM(item){
    this.navCtrl.push('AddlokasirsPage', item);
  }

  editRM(item){
    	this.navCtrl.push('EditrmPage', item);
  }
}
