import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailrmPage } from './detailrm';

@NgModule({
  declarations: [
    DetailrmPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailrmPage),
  ],
})
export class DetailrmPageModule {}
