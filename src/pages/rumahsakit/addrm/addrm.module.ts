import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddrmPage } from './addrm';

@NgModule({
  declarations: [
    AddrmPage,
  ],
  imports: [
    IonicPageModule.forChild(AddrmPage),
  ],
})
export class AddrmPageModule {}
