import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {ServicesProvider} from '../../../providers/services/services';
import firebase from 'firebase';
/**
 * Generated class for the AddrmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addrm',
  templateUrl: 'addrm.html',
})
export class AddrmPage {
  form: any;
  errorMessage: any;

  fileName: any;
  storageRef: any;
  uploadTask: any;
  downloadURL: any;
  selectedFile: any;

  disableSubmit: boolean = false;

  constructor(
    public service: ServicesProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.form={};
  }

  onChange(event){
    this.selectedFile = event.target.files[0];
    this.disableSubmit = true;
    console.log(this.selectedFile);
    this.upLoad();
  }

  upLoad(){
  var fileName = this.selectedFile.name;
  var storageRef = firebase.storage().ref('rumah-sakit/' + fileName);
  var metadata = {contentType: 'image/jpeg'};
  var uploadTask = storageRef.put(this.selectedFile, metadata);
  uploadTask.on('state_changed', (snapshot) =>{
    console.log(snapshot);
    var progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      console.log('upload' + progress + '% done');
    switch(uploadTask.snapshot.state){
      case firebase.storage.TaskState.PAUSED: 
        console.log('upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING:
        console.log('upload is running');
        break;  
    }
    }, (error) =>{
        console.log(error);
      }, () =>{
        this.downloadURL = uploadTask.snapshot.downloadURL;
        this.disableSubmit = false;
        console.log(this.downloadURL);
        console.log("successfully uploaded");
  });
  }

  saveRS(form){
    if(this.validate()){
    this.service.addRm(this.downloadURL, this.form.name, this.form.description, this.form.teleponcc, this.form.teleponapotik, this.form.openclose)
       .then(() =>{
         console.log('saved');
         this.viewCtrl.dismiss();
       });

      }
  }
  validate(){
    if(this.form.name == undefined || this.form.name == ''){
     this.errorMessage = 'Silahkan isi nama rumah sakit';
     return false;
   }

   if(this.form.description == undefined || this.form.description == ''){
     this.errorMessage = 'Mohon isi keterangan rumah sakit';
     return false;
   }

   if(this.form.teleponcc == undefined || this.form.teleponcc == ''){
     this.errorMessage = 'Silahkan isi telepon rumah sakit';
     return false;
   }

   if(this.form.teleponapotik == undefined || this.form.teleponapotik == ''){
     this.errorMessage = 'Silahkan isi telepon apotik rumah sakit';
     return false;
   }

   if(this.form.openclose == undefined || this.form.openclose == ''){
     this.errorMessage = 'Silahkan isi Waktu buka dan tutup rumah sakit';
     return false;
   }

   if(this.downloadURL == undefined || this.downloadURL == ''){
     this.errorMessage = 'Silahkan upload foto rumah sakit';
     return false;
   }
  
   return true;
 }
 
 dismiss(){
  this.viewCtrl.dismiss();
}
}
