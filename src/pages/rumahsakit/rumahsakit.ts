import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ValuesProvider } from '../../providers/values/values';
import firebase from 'firebase';
import { CommonServicesProvider } from '../../providers/common-services/common-services';
/**
 * Generated class for the RumahsakitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rumahsakit',
  templateUrl: 'rumahsakit.html',
})
export class RumahsakitPage {

  public rumahsakit:Array<any>;
  public loadedrmList:Array<any>;
  public rmRef:firebase.database.Reference;
  
  constructor(
    public nav: NavController, 
    public params: NavParams, 
    private alertCtrl: AlertController,
    public service: ServicesProvider, 
    public values:ValuesProvider,
    public modalCtrl: ModalController,
    public commonService:CommonServicesProvider
  ) {

    firebase.database().ref('/rs')
    .on('value', rmList => {
      let items = [];
      rmList.forEach( item => {
        items.push(item.val());
        return false;
      });
    
      this.rumahsakit = items;
      console.log(this.rumahsakit);
      this.loadedrmList = items;
    });

  
    this.initializeItems();
  }

  initializeItems(): void {
    this.rumahsakit = this.loadedrmList;
  }

  cariRS(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.rumahsakit = this.rumahsakit.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.rumahsakit.length);
  
  }
  detail(item){
    this.nav.push('DetailrmPage', item);
  }
  tambah(){
    let modal = this.modalCtrl.create('AddrmPage');
    modal.present();
    modal.onDidDismiss((res)=>{
    });
  }
  rubah(item){
    let modal = this.modalCtrl.create('EditrmPage', item);
    modal.present();
    modal.onDidDismiss((res)=>{
    });
  }
  lokasi(item){
    this.nav.push('LokasirsPage', item);
  }
  addlokasi(item){
    this.nav.push('AddlokasirsPage', item);
  }
  hapus(id){   
    let alert = this.alertCtrl.create({
      title: "Hapus",
      message: 'Anda yakin mau menghapus Rumah Sakit ini?',
      buttons: [
        {
          text: 'tidak',
          role: 'cancel',
          handler: () => {    

          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.service.delRm(id);
          }
        }
      ]
    });
    alert.present();
  }
}
