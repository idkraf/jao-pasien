import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatKurirPage } from './chat-kurir';

@NgModule({
  declarations: [
    ChatKurirPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatKurirPage),
  ],
})
export class ChatKurirPageModule {}
