import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from "firebase";
import { ChatProvider } from '../../providers/chat/chat';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the ChatKurirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-kurir',
  templateUrl: 'chat-kurir.html',
})
export class ChatKurirPage {
  adminID:string = '';
  adminName:string = '';
  kurirID:string = '';
  kurirName:string = '';
  message:string = '';
  messages: object[];

  constructor(
    private chat:ChatProvider,
    private auth:AuthProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public db: AngularFireDatabase
  ) {
    this.kurirID = this.navParams.get('kurirID');
    this.kurirName = this.navParams.get('kurirName');
    this.adminID = this.navParams.get('adminID');
    this.adminName = this.navParams.get('adminName');
    this.db.list('/support/'+this.adminID+'/'+this.kurirID+'/').valueChanges().subscribe(data => {
      this.messages = data
    });

  }

  sendMessage(){
    this.chat.adminupdateBadgeKurir(this.kurirID);
    this.db.list('/support/'+this.adminID+'/'+this.kurirID+'/').push({
      uid: this.adminID,
      userName: this.adminName,
      message: this.message,
      timestamp:firebase.database.ServerValue.TIMESTAMP
    }).then(() => {
      this.message = ''
    })
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }

}
