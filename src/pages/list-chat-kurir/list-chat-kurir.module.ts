import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListChatKurirPage } from './list-chat-kurir';

@NgModule({
  declarations: [
    ListChatKurirPage,
  ],
  imports: [
    IonicPageModule.forChild(ListChatKurirPage),
  ],
})
export class ListChatKurirPageModule {}
