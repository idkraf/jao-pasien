import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../../providers/auth/auth';
import { ValuesProvider } from '../../../providers/values/values';
import { ServicesProvider } from '../../../providers/services/services';
import firebase from 'firebase';
/**
 * Generated class for the TeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team',
  templateUrl: 'team.html',
})
export class TeamPage {

  public adminList:Array<any>;
  public loadedadminList:Array<any>;
  currentUser: any;
  adminmaster:any;
  _showSignup: boolean = true;

  constructor(
    public auth: AuthProvider,
    public values:ValuesProvider,  
    public service: ServicesProvider,
    public navCtrl: NavController, 
    public navParams: NavParams) {

    this.currentUser = firebase.auth().currentUser;
    //this.adminmaster = {};
    this.service.getAdminProfile(this.currentUser.uid).on('value', snapshot =>{
      if(snapshot.val()){
        this.adminmaster = snapshot.val().role;
      }  
    });

    firebase.database().ref('/admin').on('value', adminList => {
      let items = [];
      adminList.forEach( item => {
        items.push(item.val());
        return false;        
      });

      this.adminList = items;
      this.loadedadminList = items;

    });
    this.initializeItems();
  }

  initializeItems(): void {
    this.adminList = this.loadedadminList;
  }

  cariAdmin(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
  
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
  
  
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
  
    this.adminList = this.adminList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.adminList.length);
  
  }

  edit(admin){
    this.navCtrl.push('EditteamPage', admin);
  }

  changepassword(email){
    this.auth.forgotPass(email).then(() =>{
      return this.navCtrl.push(TeamPage);
    });
  }

  addAdmin(){    
    this.auth.logoutUser().then(()=>{
      this.values.isLoggedIn = false;
      this.values.isRegister = true;
//      this._showSignup = true;
      this.navCtrl.setRoot('LoginPage');
    });
    //this.navCtrl.setRoot('AddteamPage');
//      console.log(this._showSignup);

  }

}
