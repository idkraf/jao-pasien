import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../../providers/services/services';
import { ValuesProvider } from '../../../providers/values/values';
/**
 * Generated class for the EditteamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editteam',
  templateUrl: 'editteam.html',
})
export class EditteamPage {
  admin:any={};
  errorMessage: any;
  constructor(
    public values:ValuesProvider,  
    public service: ServicesProvider,
    public navCtrl: NavController, public navParams: NavParams) {

    this.admin = navParams.data;
  }
  saveAdmin(){
    this.service.editAdmin(this.admin.name, this.admin.phoneNo, this.admin.id)
    .then(() =>{
      this.navCtrl.pop();
     }).catch( (error) => {this.handleErrors(error);
      });
  }
  handleErrors(err){
    console.log(err.code);
    this.errorMessage = err.message;
  }

}
