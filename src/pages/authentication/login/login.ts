import { Component, NgZone } from '@angular/core';
import { AuthProvider } from '../../../providers/auth/auth';
import { LoadingController, AlertController} from 'ionic-angular';
// import { Facebook } from '@ionic-native/facebook';
//import { GooglePlus } from '@ionic-native/google-plus';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
//import { TwitterConnect } from '@ionic-native/twitter-connect';
import { ValuesProvider } from '../../../providers/values/values';
import { ServicesProvider } from '../../../providers/services/services';

import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  error: any;
  zone: NgZone;
  form: any; 

  userProfile: any = null;
  isLoggedIn: boolean = false;
  isRegister: boolean = false;
  adminList:any;
  currentUser: any;
  userProfiles: any = null;
  role: any;

  errorRegisterMessage: any;
  errorSigninMessage: any;
  signup: boolean = false;
  _showSignup: boolean = false;
 
  constructor(
    public nav: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider, 
    public afAuth: AngularFireAuth,
    public loadingCtrl: LoadingController, 
    /* private fb: Facebook, private twitter: TwitterConnect, private googlePlus: GooglePlus,*/ 
    public alertCtrl:AlertController, 
    public values:ValuesProvider,  
    public service: ServicesProvider) {
    
    this.role = "Admin";
    this.currentUser = firebase.auth().currentUser;
    this.form = {};
    this.auth = auth;
    this.adminList = firebase.database().ref('/admin');
    this.zone = new NgZone({});  
  }

  ionViewWillEnter() {

  }

  showSignup(){
    this._showSignup = true;
    this.values.isRegister = true;
  }

  hideSignup(){
    this._showSignup = false;
    this.values.isRegister = false;
  }

  //EMAIL AND PASSWORD LOGIN

  login(){
    if(this.validate()){
      //this.disableLogin = true;
        this.auth.login(this.form.email, this.form.password).then((success) =>{

          this.userProfile = success;

          this.service.getAdminProfile(this.userProfile.uid).on('value', (snapshot) =>{
            if (snapshot.exists()) {              
              this.values.isLoggedIn = true;
              console.log(this.values.isLoggedIn);

              this.userProfiles = snapshot.val();
              this.values.id = this.userProfile.uid;
              console.log(this.values.id);

              this.values.userRole = firebase.database().ref('/Admin-Role').child(this.userProfile.uid).on('value', snapshot =>{
                if(snapshot.val()){
                  this.values.userRole = snapshot.val().role;
                }
                console.log(this.values.userRole);
              });
    
             // this.nav.setRoot('TabsPage');

            }else{
              this.values.isLoggedIn = false;
              firebase.auth().signOut();
              this.nav.setRoot(LoginPage);
              alert("Bukan akun administrator.");
            }
          }); 
        }).catch(err => {this.handleError(err)});
      }
  }
  handleError(err){
        console.log(err.code);
        this.errorSigninMessage = err.message;
  }
  validate(){
    if(this.form.email == undefined || this.form.email == ''){
      this.errorSigninMessage = 'Please enter email';
      return false;
    }
    if(this.form.password == undefined || this.form.password == ''){
      this.errorSigninMessage = 'Please enter password';
      return false;
    }
    return true;
  }
  forgotten(){
    this.nav.push('ResetpassowrdPage');
  }
  logOut(){
    this.auth.logoutUser().then(() => {
      this.values.isLoggedIn = false;
      this.values.userRole = 'null';
      console.log(this.values.isLoggedIn = false);
      console.log(this.values.id = null);
    });
  }
  address(item){
    console.log(item)
    this.nav.push('Address', item)
  }

  profil(item){
    console.log(item)
    this.nav.push('ProfilPage', item)
  }

  myOrder(){
    this.nav.push('MyorderPage');
  }

  register() {
    if(this.validateRegister(this.form)){
      this.auth.register(this.form.email, this.form.password, this.form.firstName, this.form.phone)
      .then(() => {
        this.currentUser = firebase.auth().currentUser;   
        this.service.getAdminProfile(this.currentUser.uid).on('value', (snapshot) =>{
           this.userProfiles = snapshot.val();
        });
        this.nav.setRoot('TabsPage');
      }).catch(err => {this.handleRegisterError(err)});
    } 
  }

  handleRegisterError(err){
    console.log(err.code);
    this.errorRegisterMessage = err.message;
  }
  validateRegister(form){
    if(this.form.firstName == undefined || this.form.firstName == ''){
      this.errorRegisterMessage = 'Please enter first name';
      return false;
    }
    if(this.form.phone == undefined || this.form.phone == ''){
      this.errorRegisterMessage = 'Mohon Massukkan nomor telepon';
      return false;
    }
    if(this.form.email == undefined || this.form.email == ''){
      this.errorRegisterMessage = 'Please enter email';
      return false;
    }
    if(this.form.password == undefined || this.form.password == ''){
      this.errorRegisterMessage = 'Please enter password';
      return false;
    }
    return true;
  }
}
