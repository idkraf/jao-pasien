/**
 * Created by a4p2 on 8/27/2017.
 */
import {Location} from './location';
export class Order{
  public static readonly NoResponseStatus : string = 'noResponse' ;
  public static readonly PendingStatus : string = 'pending' ;
  public static readonly RejectedStatus : string = 'rejected' ;
  public static readonly DeliveredStatus : string = 'delivered' ;
  public static readonly AssignAllDist : string = 'allDist' ;
  public static readonly AssignSpecificDist : string = 'specificDist' ;
  public static readonly ordersToAllDistCreatedEvent : string = 'ordersToAllDist:created';
  public static readonly ordersToAllDistRemovedEvent : string = 'ordersToAllDist:removed';
  public static readonly ordersToSpecificDistCreatedEvent : string = 'ordersToSpecificDist:created';
  public static readonly ordersToSpecificDistRemovedEvent : string = 'ordersToSpecificDist:removed';
  public static readonly distHistoryChangeEvent : string = 'distHistory:change' ;
  public static readonly pasienHistoryChangeEvent : string = 'pasienHistory:change' ;
  private _assignDistType : string ;
  private _assignDistID : string ;
   constructor(
    private  _item : string ,    
    private  _pasienID : string ,
    private  _invo : string ,
    private  _kodeinvoice : string ,   
    private  _ongkir : string ,
    private  _komisi : string ,
    private  _total : string ,
    private  _totalproduk : string ,
    private  _voucher : string,
    private  _location : Location ,
    private  _paymentType : string ,
    private  _userProfiles : string ,
    private  _status ?: string ,
    private  _distributerID ?: string ,
    private  _kurirID ?: string ,
    private  _orderID ?: string,
    private _rateValPasien?: number,
    private _rateValKurir?: number
  ) {
  }
  get assignDistID():string {
    return this._assignDistID;
  }
  set assignDistID(val : string) {
    this._assignDistID = val;
  }
  get assignDistType():string {
    return this._assignDistType;
  }
  set assignDistType(val : string) {
    this._assignDistType = val;
  }
  get userProfiles():string {
    return this._userProfiles;
  }
  set userProfiles(val : string) {
    this._userProfiles = val;
  }
  get pasienID():string {
    return this._pasienID;
  }
  set pasienID(val : string) {
    this._pasienID = val;
  }
  get item():string {
   return this._item;
  }
  set item(val : string) {
    this._item = val;
  }
  get invo():string {
    return this._invo;
  }  
  set invo(val : string) {
    this._invo = val;
  }
  get kodeinvoice():string {
    return this._kodeinvoice;
  }  
  set kodeinvoice(val : string) {
    this._kodeinvoice = val;
  }
  get ongkir():string {
    return this._ongkir;
  }  
  set ongkir(val : string) {
    this._ongkir = val;
  }
  get komisi():string {
    return this._komisi;
  }  
  set komisi(val : string) {
    this._komisi = val;
  }
  get voucher():string {
    return this._voucher;
  }  
  set voucher(val : string) {
    this._voucher = val;
  }
  //  total: number = 0.00;
  get total():string {
    return this._total;
  }
  set total(val : string) {
    this._total = val;
  }
  get totalproduk():string {
    return this._totalproduk;
  }
  set totalproduk(val : string) {
    this._total = val;
  }
  get location():Location {
    return this._location;
  }
  set location(val : Location) {
    this._location = val;
  }
  get paymentType():string {
    return this._paymentType;
  }
  set paymentType(val : string) {
    this._paymentType = val;
  }
  get status():string {
    return this._status;
  }
  get distributerID(): string {
    return this._distributerID;
  }
  get kurirID(): string {
    return this._kurirID;
  }
  get orderID():string {
    return this._orderID;
  }
  get rateValPasien():number {
    return this._rateValPasien;
  }
  get rateValKurir():number {
    return this._rateValKurir;
  } 
}


