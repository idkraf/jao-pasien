export class User{
	private _name : string ;
	private _phoneNo : number ;
	private _email : string ;
	private _id : string ;

	constructor(){
	}
	get name():string {
		return this._name;
	  }
	  get phoneNo():number {
		return this._phoneNo;
	  }
	  get email():string {
		return this._email;
	  }
	  get id(): string {
		return this._id;
	  }
	
}