import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from "firebase";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  public fireAuth: any;
  public adminList:any;

  constructor(public http: HttpClient) {
    console.log('Hello AuthProvider Provider');
    this.fireAuth = firebase.auth();
    this.adminList = firebase.database().ref('/admin'); 
  }


  getUid(){
    let usr = firebase.auth().currentUser;
    return usr.uid;
    
  }
  
  login(email: String, password: String){
    return this.fireAuth.signInWithEmailAndPassword(email,password);
  }  
  
  register(email:String, password:String, firstname:String, phone:String){
    return this.fireAuth.createUserWithEmailAndPassword(email, password)
    .then((user) =>{
      this.adminList.child(user.uid).set({
        id: user.uid,
        email: email,
        name: firstname,
        phoneNo: phone
        });
    });  
  }

  forgotPass(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser(): any {
    return this.fireAuth.signOut();
  }

}
