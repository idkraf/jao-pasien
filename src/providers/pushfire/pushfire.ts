import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FCM } from '@ionic-native/fcm';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';
//import {User} from "../models/user";

declare var FCMPlugin: any;

interface FCMPlugin {
  initializeRemoteConfig: (firstParam: any, secondParam: any) => void;
  getStringValueForKey: (firstParam: any, secondParam: any, thirdParam: any) => void;
  getToken: (firstParam: any, secondParam: any) => void;
  subscribeToTopic: (firstParam: string) => void;
  unsubscribeFromTopic: (firstParam: string) => void;
  onNotification: (firstParam: any, secondParam: any, thirdParam: any) => void;
  logEvent: (firstParam: any, secondParam: any, thirdParam: any, fourthParam: any) => void;
  setUserId: (firstParam: any, secondParam: any, thirdParam: any) => void;
  setUserProperty: (firstParam: any, secondParam: any, thirdParam: any, fourthParam: any) => void;
}


@Injectable()
export class PushfireProvider {
  //public user = User;
  userId:any;

  constructor(
    private toastCtrl: ToastController,
    private notificationManager: Events,
    public http: HttpClient,
    private afdb:AngularFireDatabase, 
    private afAuth:AngularFireAuth,
    private fcm: FCM,
  ) {   

    firebase.auth().onAuthStateChanged(user=>{
      if (user) {
         this.userId = user.uid 
        };
      console.log(this.userId);
    });
    
  }

  //Remote Config
  initializeRemoteConfig() {
    FCMPlugin.initializeRemoteConfig((result) => { }, (error) => { });
  }

  getStringValueForKey(remoteConfigKey: string, successCallback: any, errorCallback: any) {
    FCMPlugin.getStringValueForKey(remoteConfigKey, successCallback, errorCallback);
  }

  //Push notification
  configureFirebasePush() {
    try {
      FCMPlugin.getToken((token) => {
        console.log('received push token', token);

        this.afdb.object('pasiens/'+this.userId).update({
          token:token
        })

       this.registerNotification();
      }, (err) => {
        console.log('error retrieving token: ' + err);
      }
      );
    }
    catch (e) {

    }
  }

  registerNotification() {
    FCMPlugin.onNotification((data) => {
      this.notificationManager.publish('app:pushnotify', data);
    }, (msg) => {
      console.log('onNotification callback successfully registered: ' + msg);
    }, (err) => {
      console.log('Error registering onNotification callback: ' + err);
    }
    );
  }
  subscribeToUser(id= this.userId) {
    // TODO: subscribe to user and handle so user tags send notifications
    try {
      FCMPlugin.subscribeToTopic(id);
    }
    catch (e) {

    }
  }

  // Subscribe topic for Push
  subscribeTopic(id= this.userId) {
    try {
      FCMPlugin.subscribeToTopic(id);
    }
    catch (e) {

    }
  }

  unsubscribeTopic() {
    FCMPlugin.unsubscribeFromTopic(this.userId);
  }

  setUserId(userId: string) {
    FCMPlugin.setUserId(userId, (success) => {
      console.log("set userId Success");
    }, (error) => {
      console.log("set userId Failure");
    });
  }

  setUserProperty(propertyString: string, propertyName: string) {
    FCMPlugin.setUserProperty(propertyString, propertyName, (success) => {
      console.log("Set User Property Success");
    }, (error) => {
      console.log("Set User Property Failure");
    });
  }

  logEvent(key: string, value: string) {
    FCMPlugin.logEvent(key, value, (success) => {
      console.log("Event Success");
    }, (error) => {
      console.log("Event Failure");
    });
  }


  saveNotifstoPasien(pasienID, title, message){
    firebase.database().ref('user_notifications').child(pasienID).push({
      message: message,
      title: title,
      isDisplayed: false,
      timestamp: firebase.database.ServerValue.TIMESTAMP
    });
  }

  saveNotifstoSeller(sellerID, title, message){
    firebase.database().ref('user_notifications').child(sellerID).push({
      message: message,
      title: title,
      isDisplayed: false,
      timestamp: firebase.database.ServerValue.TIMESTAMP
    });
  }

  sendNotiftoKurir(title, message, kurirID){
    let body = {
      "notification": {
        "title": title,
        "body": message,
        "sound": "default",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": {
        "param1": "",
        "param2": "",
      },
      "to": `/topics/${kurirID}`,
      "priority": "high",
      "restricted_package_name": "kurir.fastfood.com"
    };

    let options = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post("https://fcm.googleapis.com/fcm/send", body, {
      headers: options.set('Authorization', 'key=AAAAGfVYZSc:APA91bFvxfL66WooVqy0IqkGde-g8cWuWSXEahTNJudv2FQULOLwrERwGWs-uOjElJeqVZtIrYaXrcN2BGLPVftHdBCa6KlRFX1X6rUiSVZfA2GC1fKi5w4TAEtHQfiR7dMXl1ccOzPprSQEmR1iDvXPql1-VgOiAg'),
    }).subscribe();
  }

  sendNotiftoSeller(title, message, sellerID){
    let body = {
      "notification": {
        "title": title,
        "body": message,
        "sound": "default",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": {
        "param1": "",
        "param2": "",
      },
      "to": `/topics/${sellerID}`,
      "priority": "high",
      "restricted_package_name": "mitra.fastfood.com"
    };

    let options = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post("https://fcm.googleapis.com/fcm/send", body, {
      headers: options.set('Authorization', 'key=AAAAGfVYZSc:APA91bFvxfL66WooVqy0IqkGde-g8cWuWSXEahTNJudv2FQULOLwrERwGWs-uOjElJeqVZtIrYaXrcN2BGLPVftHdBCa6KlRFX1X6rUiSVZfA2GC1fKi5w4TAEtHQfiR7dMXl1ccOzPprSQEmR1iDvXPql1-VgOiAg'),
    }).subscribe();
  }


  sendNotiftoAdmin(title, message){
    let body = {
      "notification": {
        "title": title,
        "body": message,
        "sound": "default",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": {
        "param1": "",
        "param2": "",
      },
      "to": "/topics/all",
      "priority": "high",
      "restricted_package_name": "admin.fastfood.com"
    };

    let options = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post("https://fcm.googleapis.com/fcm/send", body, {
      headers: options.set('Authorization', 'key=AAAAGfVYZSc:APA91bFvxfL66WooVqy0IqkGde-g8cWuWSXEahTNJudv2FQULOLwrERwGWs-uOjElJeqVZtIrYaXrcN2BGLPVftHdBCa6KlRFX1X6rUiSVZfA2GC1fKi5w4TAEtHQfiR7dMXl1ccOzPprSQEmR1iDvXPql1-VgOiAg'),
    }).subscribe();
  }

  sendNotiftoAllKurir(title1, message1){
    let body = {
      "notification": {
        "title": title1,
        "body": message1,
        "sound": "default",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": {
        "param1": "",
        "param2": "",
      },
      "to": "/topics/all",
      "priority": "high",
      "restricted_package_name": "kurir.fastfood.com"
    };

    let options = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post("https://fcm.googleapis.com/fcm/send", body, {
      headers: options.set('Authorization', 'key=AAAAGfVYZSc:APA91bFvxfL66WooVqy0IqkGde-g8cWuWSXEahTNJudv2FQULOLwrERwGWs-uOjElJeqVZtIrYaXrcN2BGLPVftHdBCa6KlRFX1X6rUiSVZfA2GC1fKi5w4TAEtHQfiR7dMXl1ccOzPprSQEmR1iDvXPql1-VgOiAg'),
    }).subscribe();
  }
}
