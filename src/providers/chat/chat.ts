import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from "firebase";
/*
  Generated class for the ChatProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChatProvider {
  public fireDatabase : any;
  public fireAuth : any;

  constructor(public http: HttpClient) {
    this.fireAuth = firebase.auth();
    this.fireDatabase = firebase.database();
    console.log('Hello ChatProvider Provider');
  }

  // APP KURIR
  //kurir kirim pesan. update ctbr ke true
  updateBadgeKurir(orderID:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/history/'+orderID+'/');
      promises.push(orderRef.child('ctkr').set(true).catch((err)=>reject(err)));
    });
    return promise;
  }

  // kurir buka pesa. update ctbr ke false
  bacaBadgeKurir(orderID:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/history/'+orderID+'/');
      promises.push(orderRef.child('ctbr').set(false).catch((err)=>reject(err)));
    });
    return promise;
  }
  

  // APP PASIEN
  //Pasien kirim pesan. update ctbr ke true
  updateBadgePasien(orderID:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/history/'+orderID+'/');
      promises.push(orderRef.child('ctbr').set(true).catch((err)=>reject(err)));
    });
    return promise;
  }


  // Pasien buka pesan. update ctbr ke false (app pasien)
  bacaBadgePasien(orderID:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/history/'+orderID+'/');
      promises.push(orderRef.child('ctkr').set(false).catch((err)=>reject(err)));
    });
    return promise;
  }
  //END CHAT ANTARA KURIR DAN PASIEN

  //kurir baca badge admin (app kurir)
  kurirbacaBadgeAdmin(id:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/kurir/'+id+'/');
      promises.push(orderRef.child('ctad').set(false).catch((err)=>reject(err)));
    });
    return promise;
  }
  //add badge kurir kirim pesan ke admin
  kurirupdateBadgeAdmin(id:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/kurir/'+id+'/');
      promises.push(orderRef.child('ctbr').set(true).catch((err)=>reject(err)));
    });
    return promise;
  }
  //buat status for orderbychild
  addMessagetoAdmin(id:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/kurir/'+id+'/');
      promises.push(orderRef.child('chatToAdmin').set(true).catch((err)=>reject(err)));
    });
    return promise;
  }


    //pasien baca badge admin (app pasien)
    pasienbacaBadgeAdmin(id:string): Promise<boolean>
    {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
        let orderRef = this.fireDatabase.ref('/pasien/'+id+'/');
        promises.push(orderRef.child('ctad').set(false).catch((err)=>reject(err)));
      });
      return promise;
    }
    //pasien add message and update badge admin (app pasien)
    pasienupdateBadgeAdmin(id:string): Promise<boolean>
    {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
        let orderRef = this.fireDatabase.ref('/pasien/'+id+'/');
        promises.push(orderRef.child('ctbr').set(true).catch((err)=>reject(err)));
      });
      return promise;
    }


    // WEB ADMIN 
    //admin baca badge pasien (web admin)
    adminbacaBadgePasien(id:string): Promise<boolean>
    {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
        let orderRef = this.fireDatabase.ref('/pasien/'+id+'/');
        promises.push(orderRef.child('ctbr').set(false).catch((err)=>reject(err)));
      });
      return promise;
    }
    //admin add message and update badge pasien (web admin)
    adminupdateBadgePasien(id:string): Promise<boolean>
    {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
        let orderRef = this.fireDatabase.ref('/pasien/'+id+'/');
        promises.push(orderRef.child('ctad').set(true).catch((err)=>reject(err)));
      });
      return promise;
    }

      //admin baca badge kurir (web admin)
  adminbacaBadgeKurir(id:string): Promise<boolean>
  {
    let promises : Promise<boolean | void>[] = [] ;
    let promise = new Promise<any>((resolve, reject) => {
      let orderRef = this.fireDatabase.ref('/kurir/'+id+'/');
      promises.push(orderRef.child('ctbr').set(false).catch((err)=>reject(err)));
    });
    return promise;
  }

    //admin add message and update badge kurir (web admin)
    adminupdateBadgeKurir(id:string): Promise<boolean>
    {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
        let orderRef = this.fireDatabase.ref('/kurir/'+id+'/');
        promises.push(orderRef.child('ctad').set(true).catch((err)=>reject(err)));
      });
      return promise;
    }
}