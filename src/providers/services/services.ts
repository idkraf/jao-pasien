import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import firebase from 'firebase';
import * as firebase from "firebase";
import 'rxjs/add/operator/map';
import {User} from "../../models/user";
/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServicesProvider {

  public fireDatabase : any;

  //Cart
  product_id: Array<number> = [];
  total: number = 0.00; //total produk
  totals: number = 0.00; // total order
  ongkir: number = 0.00; // ongkos kirim
  komisi: number = 0.00; // komisi
  voucher:number = 0.00; // voucher

  proqty: Array<number> = [];
  locname:string;
  lat:number;
  lng:number;
  
  //Main
  RumahSakitList:any;
  adminList: any;

  //construc
  public cart:any;
  public setting:any;
  public bannerList: any;
  public bantuan:any;
  public pasien:any;
  public kurir:any;
  public admin:any;
  public rmssakit:any;
  public order:any;
  public komisiUndoneList: any;
  public komisiDoneList: any;
  public setoranList:any;
  public kuponList:any;
  public aboutInfo:any;
  public termInfo:any;
  public termKurir:any;

  constructor(public http: HttpClient) {
    this.fireDatabase = firebase.database();
    this.cart = { "line_items": [], };
    this.setting = firebase.database().ref('/Setting'); //1
    this.bannerList = firebase.database().ref('/Banners'); //2
    this.bantuan = firebase.database().ref('/bantuan'); //3
    this.pasien = firebase.database().ref('/pasien'); //4
    this.kurir = firebase.database().ref('/kurir'); //5
    this.admin = firebase.database().ref('/admin'); //6
    this.rmssakit = firebase.database().ref('/rs');//7
    this.order =  firebase.database().ref('/history');//8
    this.komisiUndoneList = firebase.database().ref('/Komisi-Undone');  //9
    this.komisiDoneList = firebase.database().ref('/Komisi-Done');  //10
    this.setoranList = firebase.database().ref('/setoran'); //11
    this.kuponList = firebase.database().ref('/kupon'); //12
    this.aboutInfo = firebase.database().ref('/about'); //13
    this.termInfo = firebase.database().ref('/term'); //14
    this.termKurir = firebase.database().ref('/term-kurir'); //15
  }

  //Setting
  getSetting(){
    return this.setting;
  }
  addSettting(form, distance){
    return this.setting.update({
      currency: form.currency,
      ongkir: form.ongkir,
      invoice: form.invoice,
      kodeinvoice: form.kodeinvoice,
      perkomisi: form.perkomisi,
      distance: distance
    });
  }
  updateOngkir(distance, addongkir, ongkir, perkomisi){
    return this.setting.update({
      distance:distance,
      addongkir:addongkir,
      ongkir:ongkir,
      perkomisi:perkomisi
    });
  }
  updateInvoice(currency, kodeinvoice, invoice){
    return this.setting.update({
      currency:currency,
      kodeinvoice:kodeinvoice,
      invoice: invoice
    });
  }

  //abaout
  getAbout(): any{
    return this.aboutInfo;
  }
  addAbout(title,keterangan){
    return this.aboutInfo.set({
    name: title,
    description: keterangan
    });
  }

  //terms
  getTerms(): any{
    return this.termInfo;
  }
  addTerms(title,keterangan){
    return this.termInfo.set({
    name: title,
    description: keterangan
    });
  }
  //terms Kurir
  getTermsKurir(): any{
    return this.termKurir;
  }
  addTermsKurir(title,keterangan){
    return this.termKurir.set({
    name: title,
    description: keterangan
    });
  }
  // kode voucher / kupon
  getKuponList(): any {
    return this.kuponList;
  }
  addKupon(name:String, kode:String, qty:String, voucher:String){
    return this.kuponList.push({
      name: name,
      kode: kode,
      qty:qty,
      voucher:voucher
    }).then( newKupon => {
      this.kuponList.child(newKupon.key).child('id').set(newKupon.key);
    });
  }
  editKupon(name:string, kode:string, qty:number, voucher:string, id){
    return this.kuponList.child(id).update({
      name: name,
      kode: kode,
      qty:qty,
      voucher:voucher
    });
  }
  delKupon(id){
    return this.kuponList.child(id).remove();
  }
  
  //Banner
  getBanner(): any{
    return this.bannerList;
  }
  addBanner(banners){
    return this.bannerList.set({
    banners1: banners[0],
    banners2: banners[1],
    banners3: banners[2]
    });
  }


  //BANTUAN
  getBantuan(): any{
    return this.bantuan;
  }
  addBantuan(name:String, description:String){
    return this.bantuan.push({
      name:name,
      description:description
    }).then( newBantuan => {
      this.bantuan.child(newBantuan.key).child('id').set(newBantuan);
     });
  }
  editBantuan(name:string, description:string, id){
    return this.bantuan.child(id).update({
      name:name,
      description:description
    });
  }
  delBantuan(id){
    return this.bantuan.child(id).remove();
  }

  //Kurir
  getKurir():any{
    return this.kurir;
  }
  editKurir( name:String, phoneNo:String, keterangan:String, id){
    return this.kurir.child(id).update({
      name:name,
      phoneNo:phoneNo,
      keterangan:keterangan
    });
  }
  editgambarkurir(uid:string, downloadURL:string){
    return this.kurir.child(uid).update({
      downloadURL: downloadURL
    });
  }
  delKurir(id){
    return this.kurir.child(id).remove();
  }
  getNoKurirList(): any{
    return this.kurir.orderByChild("verifikasi").equalTo(false);
  }  
  getKurirList(): any{
    return this.kurir.orderByChild("verifikasi").equalTo(true);
  }
  getSetoranKurir():any{
    return this.kurir.orderByChild("setoran").equalTo(true);
  }
  getChatKurir():any{
    return this.kurir.orderByChild("chatToAdmin").equalTo(true);
  }
  //Pasien
  getPasien():any{
    return this.pasien;
  }
  getPasienProfile():any{
    return this.pasien;
  }
  editPasien( name:String, phoneNo:String, keterangan:String, id){
    return this.pasien.child(id).update({
      name:name,
      phoneNo:phoneNo,
      keterangan:keterangan
    });
  }
  editgambarpasien(uid:string, downloadURL:string){
    return this.pasien.child(uid).update({
      downloadURL: downloadURL
    });
  }
  getChatPasien():any{
    return this.pasien.orderByChild("chatToAdmin").equalTo(true);
  }
  //admin
  getAdmin():any{
    return this.admin;
  }
  getAdminData(adminID  : string): Promise<User>{
    let promise = new Promise<any>((resolve, reject) => {
      let custRef = firebase.database().ref('/admin/'+adminID);
      custRef.once('value').then((snapshot)=>{
        resolve(<User>snapshot.val());
      }).catch((err)=>reject(err));
    });
    return promise ;
  }

  getAdminProfile(id):any{
    return this.admin.child(id);
  }
  SaveAdmin(email:String, id):any{
    return this.admin.child(id).update({
      id:id,
      email:email
    });
  }
  editAdmin(name: String, phone:String, id:any): any {
    return this.admin.child(id).update({
      name: name,
      phoneNo: phone
    });
  }
  editmyAkun(name: String, email:String, id:any): any {
    return this.admin.child(id).update({
      name: name,
      email: email
    });
  }
  // Rumah Sakit
  getRm():any{
    return this.rmssakit;
  }
  addRm(downloadURL:String, name:String, description:String, teleponcc:String, teleponapotik:String, openclose:String){
    return this.rmssakit.push({
      downloadURL:downloadURL,
      name:name,
      description:description,
      teleponcc:teleponcc,
      teleponapotik:teleponapotik,
      openclose : openclose,
      setlokasi:false
    }).then( newRumahSakit => {
      this.rmssakit.child(newRumahSakit.key).child('id').set(newRumahSakit.key);
     });
  }
  editgambarrs(uid:string, downloadURL:string){
    return this.rmssakit.child(uid).update({
      downloadURL: downloadURL
    });
  }
  addLokasiRm(lat:String, lng:String, alamat:String, id){
    return this.rmssakit.child(id).push({
      lat:lat,
      lng:lng,
      alamat:alamat
    })
  }
  editRm(downloadURL:String,name:string,description:string,teleponcc:string,teleponapotik:string,openclose:string,id){
    return this.rmssakit.child(id).update({
      downloadURL:downloadURL,
      name:name,
      description:description,
      teleponcc:teleponcc,
      teleponapotik:teleponapotik,
      openclose:openclose,
    });
  }
  delRm(id){
    return this.rmssakit.child(id).remove();
  }

  //Order
  getOrder():any{
    return this.order;
  }
  getOrderDetail(id){
    return this.order.child(id);
  }
  getOrderListRM(id){
    return this.order.orderByChild("rsID").equalTo(id);
  }
  getPasienOrderList(id){
    return this.order.orderByChild("pasienID").equalTo(id);
  }
  getKurirOrderList(id){
    return this.order.orderByChild("kurirID").equalTo(id);
  }
  getPendingOrderList(){
    return this.order.orderByChild("status").equalTo("pending");
  }
  getnoRepsonOrderList(){
    return this.order.orderByChild("status").equalTo("noResponse");
  }
  getOrderCancelList(){
    return this.order.orderByChild("status").equalTo("rejected");
  }
  getOrderDeliveredList(){
    return this.order.orderByChild("status").equalTo("delivered");
  }
  delOrder(id){
    return this.order.child(id).remove();
  }



  // KOmisi
  getUnKomisiList(id){
     return this.komisiUndoneList.child(id);
   }
 
   getKomisiList(id){
     return this.komisiDoneList.child(id);
   }
 
   getSetoran(){
      return this.setoranList;
    }



  moveSetoran(kurirID:string): Promise<boolean>
  {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {

          let undoneRef = this.fireDatabase.ref('/setoran/'+kurirID);
          let doneRef = this.fireDatabase.ref('/finance/'+kurirID);
          
          undoneRef.once('value').then((undoneSnapshot)=>{
              doneRef.set(undoneSnapshot.val(),function(error){
                  if (error) {
                      console.log("could not copy user object." + error);
                  } else {
                      console.log("user object copied successfully.");

                      //update status ada histori setoran
                      let kurirOrderRef = this.fireDatabase.ref('kurir/'+kurirID);
                      promises.push(kurirOrderRef.child('setor').set(true).catch((err)=>reject(err)));

                      // remove user object
                      undoneRef.remove();

                  }
              })
          });

          Promise.all(promises).then(()=>{
              resolve(true);
            }).catch((err)=>reject(err));
      });

      return promise;
    }

}
