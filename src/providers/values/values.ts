import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ValuesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ValuesProvider {
  
  //Values
  isLoggedIn: boolean = false;
  isRegister: boolean = false;

  currency: any = "Rp";
  hargadasar: number = 0.00; //total produk

  price: any;
  cart: Array<number> = [];
  qty: number = null;

  pasienList: any;
  kurirList: any;

  id:any;
  role: any;
  userRole: any = "";

  constructor(public http: HttpClient) {
    console.log('Hello ValuesProvider Provider');
  }

  changeRoll(role){
  
    if(role ==  "Kurir"){
      this.role = "Kurir";
    }
  
    else if(role == "Pasien"){
      this.role = "Pasien";
    }
  
    else if(role == "Admin"){
      this.role = "Admin";
    }
    console.log()
  }
}
