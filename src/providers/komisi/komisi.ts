import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as firebase from "firebase";
import {Events} from "ionic-angular";
/*
  Generated class for the KomisiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class KomisiProvider {

  public fireDatabase : any;
  public fireAuth : any;

  constructor(
      public http: HttpClient,
      public events: Events,
  ) {
    console.log('Hello OrderProvider Provider');
    this.fireAuth = firebase.auth();
    this.fireDatabase = firebase.database();
  
  }

  moveKomisi(kurirID:string): Promise<boolean>
  {
      let promises : Promise<boolean | void>[] = [] ;
      let promise = new Promise<any>((resolve, reject) => {
          let kurirOrderRef = this.fireDatabase.ref('kurir/'+kurirID);
          let setoranRef = this.fireDatabase.ref('/setoran/');
          let doneRef = this.fireDatabase.ref('/Komisi-Done/'+kurirID);
          let undoneRef = this.fireDatabase.ref('/Komisi-Undone/'+kurirID);
          undoneRef.once('value').then((undoneSnapshot)=>{
              //move to data setoran
              let setoran = [];
              undoneSnapshot.forEach((snap) =>{
                  setoran.push(snap.val())
              });
              promises.push(setoranRef.set(setoran).catch((err) => reject(err)));
              //move setoran to done
              promises.push(doneRef.set(undoneSnapshot.val()).catch((err) => reject(err)));
              //remove undone
              promises.push(undoneRef.remove().catch((err)=>reject(err)));
              //update status setor dan setoran di kurir
              promises.push(kurirOrderRef.child('setor').set(true).catch((err)=>reject(err)));
              promises.push(kurirOrderRef.child('setoran').set(false).catch((err)=>reject(err)));
          });
          Promise.all(promises).then(()=>{
              resolve(true);
          }).catch((err)=>reject(err));
      });

      return promise;
  }


}
